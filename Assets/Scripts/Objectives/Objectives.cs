﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Objectives : MonoBehaviour {

	public static float timeSinceStart;
	public static int survivors;

	public float timeBeforeEnding;
	public string endingSceneName1;
	public string endingSceneName2;

	public static int totalSpaceShipParts = 6;
	public static int partsFixed;

	[SerializeField]
	private float timeBeforeFound = 500;
	private float timePassed;

	public GoTo goTo;

	// Use this for initialization
	void Awake () {
		partsFixed = 0;
		timePassed = 0;
		timeSinceStart = 0;
		survivors = 6;
		StartCoroutine (MyUpdate ());
		Messenger.AddListener(GlobalVariables.MESSAGE_REDUCE_TIME, ReduceObjectiveTime);
		Messenger.MarkAsPermanent (GlobalVariables.MESSAGE_REDUCE_TIME);
	}

	/// <summary>
	/// Raises the destroy event.
	/// </summary>
	void OnDestroy(){
		Messenger.RemoveListener(GlobalVariables.MESSAGE_REDUCE_TIME, ReduceObjectiveTime);
	}

	void ReduceObjectiveTime () {
		timeBeforeFound = (timeBeforeFound - timePassed) * 0.75f;
		timePassed = 0;
	}

	IEnumerator MyUpdate(){
		while (timePassed < timeBeforeFound && partsFixed < totalSpaceShipParts) {
			timeSinceStart += Time.deltaTime;
			timePassed += Time.deltaTime;
			yield return 0;
		}

		yield return 0;

		if(timePassed >= timeBeforeFound)
			Messenger.Broadcast<string> (GlobalVariables.MESSAGE_ON_RECEIVE_MESSAGE, "Your signal was found!\nYou are safe!");
		else
			Messenger.Broadcast<string> (GlobalVariables.MESSAGE_ON_RECEIVE_MESSAGE, "The spaceship is completely fixed!\nYou are safe!");
		
		yield return new WaitForSeconds (timeBeforeEnding);
		Time.timeScale = 1;
		goTo.goToSceneWithTransition(timePassed >= timeBeforeFound ? endingSceneName1 : endingSceneName2);
	}
}
