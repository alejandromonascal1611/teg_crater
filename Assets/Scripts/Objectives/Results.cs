﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Results : MonoBehaviour {

	public Text gameTime;
	public Text survivors;

	// Use this for initialization
	void Awake () {
		gameTime.text = "Game Time: " + ((int)Objectives.timeSinceStart)/60 + "." + ((int)Objectives.timeSinceStart)%60/6 + " minutes";
		survivors.text = "Survivors: " + (Objectives.survivors<=0?0:Objectives.survivors) + "/6";
	}
}
