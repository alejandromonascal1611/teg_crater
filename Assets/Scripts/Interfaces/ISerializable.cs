﻿using SimpleJSON;

/// <summary>
/// An interface for serializing/deserializing a class
/// </summary>
public interface ISerializable {

	/// <summary>
	/// Deserialize
	/// </summary>
	/// <param name="jsonData">Data to be deserialized</param>
	void Deserialize(JSONNode jsonData);

	/// <summary>
	/// Serialize
	/// </summary>
	/// <returns>Data serialized</returns>
	JSONNode Serialize();
}
