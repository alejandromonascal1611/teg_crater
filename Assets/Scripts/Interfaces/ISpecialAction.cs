﻿using UnityEngine;
using System.Collections;

public abstract class ISpecialAction {

	/// <summary>
	/// Makes an Special Action
	/// </summary>
	public abstract void Act(params System.Object[] args);

}
