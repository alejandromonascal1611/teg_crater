﻿using Player;
/// <summary>
/// Interface for the interact method
/// </summary>
public interface IInteractable
{
	/// <summary>
	/// Action of interacting with an interactable object
	/// </summary>
	void Interact(bool showOnUI, PlayerInventory inventory);
}
