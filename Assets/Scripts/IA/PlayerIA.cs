﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Managers;
using Player;
using Stats;
using Inventory;

namespace IA
{
    [RequireComponent(typeof(PlayerInteract),typeof(PlayerMovement),typeof(PlayerTeleport))]
    [RequireComponent(typeof(PlayerInventory), typeof(PlayerStatsReduction),typeof(Rigidbody))]
    [RequireComponent(typeof(PlayerAnimations))]
    public class PlayerIA : MonoBehaviour
    {

        public enum Mood { Motivated, Desmotivated }
        public enum State { Moving, NeedHelp, FoundProvition, FoundHelp, OtherNeedsHelp, OtherFoundHelp, Transition, MakeAndDropSpecialItem}

        private State _state;
        private Mood _mood;
        private PlayerType _type;

        private GameObject _helpFrom;
        private PlayerType _helpType;

        private float _currentDirection;

        private PlayersManager _playersManager;
        
        private PlayerInteract _interact;
        
        private PlayerMovement _movement;

        private PlayerAnimations _animation;

        private PlayerTeleport _teleport;
        
        private PlayerInventory _inventory;
        
        private PlayerStatsReduction _stats;

        private Rigidbody _rigidbody;

		private Transform _mainCameraPos;

        private int _jumpCount;
        private bool _gravityAffected;

        private float _minReactionTime = 2f;
        private float _maxReactionTime = 3f;

        private int _itemsCount;
        private int _farmerItemCount;
        private int _chefItemCount;
        private int _medicItemCount;
        private int _mecanicItemCount;

        private bool helpAsked;
		
		[SerializeField]
		public LayerMask _floorLayers;

		public int maxDistance;

		public int farFromCameraDistance = 25;

		public float nearFromPlayerDistance = 1.5f;

		public float outOfCameraTime = 1.25f;

		void Awake()
        {
            _state = State.Moving;
            _mood = Mood.Motivated;
            _jumpCount = 0;
            _gravityAffected = false;
            _rigidbody = GetComponent<Rigidbody>();

            _interact = GetComponent<PlayerInteract>();
            _movement = GetComponent<PlayerMovement>();
            _teleport = GetComponent<PlayerTeleport>();
            _inventory = GetComponent<PlayerInventory>();
            _stats = GetComponent<PlayerStatsReduction>();
            _animation = GetComponent<PlayerAnimations>();
			_mainCameraPos = UnityEngine.Camera.main.transform;

            Messenger.AddListener<PlayerType,GameObject>(GlobalVariables.MESSAGE_ON_IA_HELP_NEEDED, OnHelpNeeded);
            Messenger.MarkAsPermanent(GlobalVariables.MESSAGE_ON_IA_HELP_NEEDED);
        }

        void OnDestroy()
        {
            Messenger.RemoveListener<PlayerType, GameObject>(GlobalVariables.MESSAGE_ON_IA_HELP_NEEDED, OnHelpNeeded);
        }

        public void Setup(PlayersManager playerManager)
        {
            _playersManager = playerManager;
        }

		bool isFarFromMainPlayer() {
			return Vector3.Magnitude(_mainCameraPos.position - transform.position) > farFromCameraDistance;
		}

		bool isNearFromPlayer() {
			return Mathf.Abs(_mainCameraPos.position.x - transform.position.x) < nearFromPlayerDistance;
		}

        void FixedUpdate()
        {
			if (_stats.isDead)
			{
				StopAllCoroutines();
				enabled = false;
				return;
			}

			if (!isFarFromMainPlayer())
			{
				_movement.Move(_currentDirection);// * (isNearFromPlayer()? 0.8f : 1));
				_animation.Move(_currentDirection);
			}
        }

		IEnumerator FakeMovement() {
			float time = outOfCameraTime / (_movement.acceleration * _movement.friction);
			Vector3 size = new Vector3(GetComponent<Renderer>().bounds.size.x * 0.5f, GetComponent<Renderer>().bounds.size.y * 0.5f, GetComponent<Renderer>().bounds.size.z * 0.5f);
			while (true) {
				yield return new WaitForSeconds(time);

				if (_stats.isDead)
					break;

				if (isFarFromMainPlayer()) {
					transform.position += Vector3.right * _currentDirection;

					int count = 0;
					while (Physics.OverlapBox(transform.position + new Vector3(0, count), size, transform.rotation, _floorLayers).Length > 0)
						count++;

					transform.position += new Vector3(0, count-1);

					if (transform.position.x > maxDistance)
						_teleport.Teleport();
				}
			}
		}

        IEnumerator ReactionUpdate() {
			StartCoroutine(FakeMovement());
            while (true) {
                yield return StartCoroutine(WaitForReaction());

				if (isNearFromPlayer())
					continue;

				if (_stats.isDead)
					break;

                switch (_state)
                {
                    case State.Moving:
                        StateMoving();
                        break;
                    case State.FoundProvition:
                        StateFoundProvition();
                        break;
                    case State.NeedHelp:
                        StateNeedHelp();
                        break;
                    case State.FoundHelp:
                        StateFoundHelp();
                        break;
                    case State.OtherNeedsHelp:
                        StateOtherNeedsHelp();
                        break;
                    case State.OtherFoundHelp:
                        StateOtherFoundHelp();
                        break;
                    case State.MakeAndDropSpecialItem:
                        StateMakeAndDropSpecialItem();
                        break;
                    case State.Transition:
                        StateTransition();
                        break;
                }

                StateAny();
                
            }
        }
        
        void StateNeedHelp() {
            if (_helpFrom != null)
            {
                if ((transform.position - _helpFrom.transform.position).magnitude > _helpFrom.transform.position.magnitude + 10)
                    _teleport.Teleport();

                _currentDirection = Mathf.Sign(transform.position.x - _helpFrom.transform.position.x);

                if ((transform.position - _helpFrom.transform.position).magnitude < 0.5f)
                    _state = State.FoundHelp;
            }
            else
            {
                _state = State.Transition;
            }

            StateMoving();
        }

        void StateFoundHelp() {
            for (int i = 0; i < _inventory._inventoryElements.Length; i++)
            {
                if (_inventory._inventoryElements[i] != null)
                {
                    for (int j = 0; j < _inventory._inventoryElements[i].dataRef.playerTypeInventoryActions.Length; j++)
                    {
                        if (_inventory._inventoryElements[i].dataRef.playerTypeInventoryActions[j] == _helpType && _inventory.timers[i] < Mathf.Epsilon)
                        {
                            /*if (_inventory.DropInventoryElement(i))
                            {
                                _inventory._inventoryElements[i] = null;
                                break;
                            }*/
                            //TODO    
                            if (!_helpFrom.GetComponent<PlayerStatsReduction>()._isCurrentPlayer && _helpFrom.GetComponent<PlayerInventory>().TryObtainInventoryElement(_inventory._inventoryElements[i])) {
                                _inventory._inventoryElements[i] = null;
                                _helpFrom.GetComponent<PlayerIA>()._state = State.Transition;
                            }
                            break;
                        }
                    }
                }
            }

            _state = State.Transition;
        }

        void StateFoundProvition()
        {
            _interact.GrabObject(false);
            _state = State.Transition;
        }

        void StateMoving() {
            if (_currentDirection < Mathf.Epsilon)
                _currentDirection = Random.value > 0.5f ? 1 : -1;
			
			if (isFarFromMainPlayer())
				return;

			if (_rigidbody.velocity.x < 0.01f)
            {
                if (_jumpCount > 3)
                {
                    if (!_gravityAffected)
                    {
                        _movement.AffectGravity();
                        _gravityAffected = true;
                        _jumpCount = 0;
                    }
                    else
                    {
                        _movement.AffectGravity();
                        _gravityAffected = false;
                        _currentDirection = 0;
                        _teleport.Teleport();
                    }
                }
                else
                {
                    _movement.Jump();
                    _animation.Jump();
                    _jumpCount++;
                }
            }
            else
            {
                if (_gravityAffected)
                {
                    _movement.AffectGravity();
                    _gravityAffected = false;
                }
                _jumpCount = 0;
            }
        }
        
        void StateOtherNeedsHelp()
        {
            StateNeedHelp();

            if (_state == State.FoundHelp)
                _state = State.OtherFoundHelp;
        }

        void StateOtherFoundHelp()
        {
            bool found = false;
            for (int i = 0; i < _inventory._inventoryElements.Length; i++)
            {
                if (_inventory._inventoryElements[i] != null && _inventory._inventoryElements[i].isUsable && (_inventory._inventoryElements[i].isSpecial || _inventory._inventoryElements[i].isAntidote))
                {
                    if (_helpFrom.GetComponent<PlayerInventory>().TryObtainInventoryElement(_inventory._inventoryElements[i]))
                    {
                        _inventory._inventoryElements[i] = null;
                    }
                    _helpFrom.GetComponent<PlayerIA>()._state = State.Transition;
                    found = true;
                    break;
                }
            }

            if (!found)
            {
                for (int i = 0; i < _inventory._inventoryElements.Length; i++)
                {
                    if (_inventory._inventoryElements[i] != null && _inventory._inventoryElements[i].isUsable && _inventory.timers[i] < Mathf.Epsilon)
                    {
                        if (_helpFrom.GetComponent<PlayerInventory>().TryObtainInventoryElement(_inventory._inventoryElements[i]))
                        {
                            _inventory._inventoryElements[i] = null;
                        }
                        _helpFrom.GetComponent<PlayerIA>()._state = State.Transition;
                        break;
                    }
                }
            }

            _state = State.Transition;
        }

        void StateMakeAndDropSpecialItem() {
            int i = StateMakeSpecialItem();

            StartCoroutine(dropWhenReady(i));
        }

        void StateMakeAndUseSpecialItem()
        {
            int i = StateMakeSpecialItem();

            StartCoroutine(useWhenReady(i));
        }

        IEnumerator dropWhenReady(int i) {
            yield return new WaitWhile(() => !_inventory._inventoryElements[i].isSpecial && !_inventory._inventoryElements[i].isRation && !_inventory._inventoryElements[i].isAntidote);

            if (_inventory.DropInventoryElement(i))
            {
                _inventory._inventoryElements[i] = null;
            }
        }

        IEnumerator useWhenReady(int i)
        {
            yield return new WaitWhile(() => !_inventory._inventoryElements[i].isSpecial && !_inventory._inventoryElements[i].isRation && !_inventory._inventoryElements[i].isAntidote);
            _inventory.UseInventoryElement(i);
        }

        bool TryToUseAntidote()
        {
            for (int i = 0; i < _inventory._inventoryElements.Length; i++)
            {
                if (_inventory._inventoryElements[i] != null && _inventory._inventoryElements[i].isUsable && _inventory._inventoryElements[i].isAntidote)
                {
                    _inventory.UseInventoryElement(i);
                    return true;
                }
            }
            return false;
        }

        bool TryToUseSpecialItem()
        {
            for (int i = 0; i < _inventory._inventoryElements.Length; i++)
            {
                if (_inventory._inventoryElements[i] != null && _inventory._inventoryElements[i].isUsable && _inventory._inventoryElements[i].isSpecial)
                {
                    _inventory.UseInventoryElement(i);
                    return true;
                }
            }
            return false;
        }

        bool TryToUseItem()
        {
            for (int i = 0; i < _inventory._inventoryElements.Length; i++)
            {
                if (_inventory._inventoryElements[i] != null && _inventory._inventoryElements[i].isUsable && _inventory.timers[i] < Mathf.Epsilon)
                {
                    _inventory.UseInventoryElement(i);
                    return true;
                }
            }
            return false;
        }

        int StateMakeSpecialItem() {
            if (_inventory.type == PlayerType.student)
                return -1;

            for (int i = 0; i < _inventory._inventoryElements.Length; i++)
            {
                if (_inventory._inventoryElements[i] != null)
                {
                    for (int j = 0; j < _inventory._inventoryElements[i].dataRef.playerTypeInventoryActions.Length; j++)
                    {
                        if (_inventory._inventoryElements[i].dataRef.playerTypeInventoryActions[j] == _inventory.type && !_inventory._inventoryElements[i].isSpecial && !_inventory._inventoryElements[i].isRation && !_inventory._inventoryElements[i].isAntidote && _inventory.timers[i] < Mathf.Epsilon)
                        {
                            _inventory.SpecialActionInventoryElement(i, null);
                            return i;
                        }
                    }
                }
            }
            return -1;
        }

        void StateTransition() {
            _state = State.Moving;

            //Make previous counting to be used in the case of a change of state
            _chefItemCount = _farmerItemCount = _itemsCount = _mecanicItemCount = _medicItemCount = 0;
            foreach (InventoryElement element in _inventory._inventoryElements)
            {
                if (element != null)
                {
                    if (!element.isSpecial && !element.isRation && !element.isAntidote)
                    {
                        foreach (PlayerType type in element.dataRef.playerTypeInventoryActions)
                        {
                            switch (type)
                            {
                                case PlayerType.cook:
                                    _chefItemCount++;
                                    break;
                                case PlayerType.doctor:
                                    _medicItemCount++;
                                    break;
                                case PlayerType.farmer:
                                    _farmerItemCount++;
                                    break;
                                case PlayerType.mechanic:
                                    _mecanicItemCount++;
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                    _itemsCount++;
                }
            }

            //Calculate probability of going in the direction of another player
            float prob = _itemsCount / 6f;
            prob *= prob;

            if (Random.value < prob)
            {
                _state = State.NeedHelp;

                //Check who to ask for help
                prob = _chefItemCount * 4 + _farmerItemCount * 5 + _mecanicItemCount * 10 + _medicItemCount * 2;
                float chefProb = _chefItemCount * 4f / prob;
                float farmerProb = chefProb + _farmerItemCount * 5f / prob;
                float mecanicProb = farmerProb + _mecanicItemCount * 10f / prob;

                prob = Random.value;

                if (prob < chefProb)
                {
                    _helpFrom = _playersManager.staticPlayersReference[1];
                    _helpType = PlayerType.cook;
                }
                else if (prob < farmerProb)
                {
                    _helpFrom = _playersManager.staticPlayersReference[0];
                    _helpType = PlayerType.farmer;
                }
                else if (prob < mecanicProb)
                {
                    _helpFrom = _playersManager.staticPlayersReference[5];
                    _helpType = PlayerType.mechanic;
                }
                else
                {
                    _helpFrom = _playersManager.staticPlayersReference[2];
                    _helpType = PlayerType.doctor;
                }

                if (_helpFrom.Equals(gameObject)) {
                    StateMakeSpecialItem();
                    _state = State.Transition;
                }

            }
        }

        void StateAny()
        {
            if (_stats.poisonLevel > 0 && !helpAsked)
            {
                if (!TryToUseAntidote())
                {
                    if (_inventory.type == PlayerType.doctor)
                    {
                        StateMakeAndUseSpecialItem();
                    }
                    else
                    {
                        Messenger.Broadcast<PlayerType, GameObject>(GlobalVariables.MESSAGE_ON_IA_HELP_NEEDED, PlayerType.doctor, gameObject);

                        helpAsked = true;
                        StartCoroutine(WaitForAsking());
                        _mood = Mood.Desmotivated;
                        /*_helpType = PlayerType.doctor;
                        if (!_playersManager.staticPlayersReference[2].GetComponent<PlayerStatsReduction>().isDead)
                        {
                            _state = State.NeedHelp;
                            _helpFrom = _playersManager.staticPlayersReference[2];
                        }
                        else
                        {
                            _helpFrom = null;
                        }*/
                    }
                }
            }

            if (!helpAsked)
            {
                for (int i = 1; i < _stats.defaultPlayerStats.size && !helpAsked; i++)
                {
                    if (_stats.consumedStats[i] / (float)_stats.defaultPlayerStats[i] < 0.25f)
                    {
                        if (!TryToUseSpecialItem() && !TryToUseItem())
                        {
                            /*_state = State.NeedHelp;
                            do {
                                _helpFrom = _playersManager.playersReference[Random.Range(0, _playersManager.playersReference.Count)];
                            }while (_helpFrom);

                            _helpType = _helpFrom.GetComponent<PlayerInventory>().type;

                            if (_helpFrom.Equals(gameObject))
                                _helpFrom = null;*/

                            PlayerType randType = (PlayerType)Random.Range((int)PlayerType.farmer, (int)PlayerType.mechanic - 2);
                            if (randType == _inventory.type)
                            {
                                randType = randType + 1;
                            }

                            Messenger.Broadcast<PlayerType, GameObject>(GlobalVariables.MESSAGE_ON_IA_HELP_NEEDED, randType, gameObject);
                            helpAsked = true;
                            StartCoroutine(WaitForAsking());
                            _mood = Mood.Desmotivated;
                        }
                    }
                }
            }

            if (!helpAsked)
                _mood = Mood.Motivated;
        }

        public IEnumerator WaitForAsking() {
            yield return new WaitForSeconds(_maxReactionTime * 10);
            helpAsked = false;
        }

        public IEnumerator OnTriggerEnter(Collider collision)
        {
            if (_state == State.Moving && collision.name.Contains("Interactable")) {
                yield return new WaitWhile(() => _rigidbody.velocity.y > Mathf.Epsilon);
                _state = State.FoundProvition;
                _currentDirection = 0;
            }
        }

        IEnumerator WaitForReaction() {
            float mult = _mood == Mood.Motivated ? 1 : 1.25f;
            yield return new WaitForSecondsRealtime(Random.Range(_minReactionTime, mult * _maxReactionTime));
        }

        void OnHelpNeeded(PlayerType playerType, GameObject who) {
            if (playerType == _inventory.type) {
                _state = State.OtherNeedsHelp;
                _helpFrom = who;
            }
        }

        public void SetEnable(bool flag)
        {
            if (flag)
                StartCoroutine(ReactionUpdate());
			else
			{
				StopAllCoroutines();
				_currentDirection = 0;
			}
        }
        
    }
}
