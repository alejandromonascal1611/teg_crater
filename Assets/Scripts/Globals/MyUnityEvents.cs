﻿using UnityEngine.Events;

/// <summary>
/// Handles Unity Events with a float parameter
/// </summary>
[System.Serializable]
public class FloatEvent : UnityEvent<float>{}
