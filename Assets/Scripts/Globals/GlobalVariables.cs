﻿/// <summary>
/// Handles all static variables
/// </summary>
public class GlobalVariables {

    /// <summary>
    /// Region for all the used tags for the game
    /// </summary>
    #region tags
		
    public static string TAG_PLAYER = "Player";
    public static string TAG_GROUND = "Ground";

    #endregion

    /// <summary>
    /// Region for all the messages used by the Messenger
    /// </summary>
    #region Messages

    public static string MESSAGE_ON_PLAYER_TOGGLE = "OnPlayerToggle";
    public static string MESSAGE_ON_PLAYER_FOCUS = "OnPlayerFocus";
    public static string MESSAGE_IS_PLAYER_ALIVE = "isPlayerAlive";
    public static string MESSAGE_ON_PLAYER_CHANGE = "OnPlayerChange";
    public static string MESSAGE_ON_PLAYER_CHANGE_STAT = "OnPlayerChangeStat";
	public static string MESSAGE_ON_PLAYER_CHANGE_INVENTORY = "OnPlayerChangeInventory";
	public static string MESSAGE_ON_UPDATE_INVENTORY = "OnUpdateInventory";
	public static string MESSAGE_ON_RECEIVE_MESSAGE = "OnReceiveMessage";
	public static string MESSAGE_REDUCE_TIME = "ReduceTime";
    public static string MESSAGE_ON_IA_HELP_NEEDED = "OnHelpNeeded";
	public static string MESSAGE_SETUP = "Setup";

	public static string badEnding = "BadEnding";

	#endregion

}
