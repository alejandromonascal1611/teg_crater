﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Stats;

namespace Inventory
{
	/// <summary>
	/// Defines the effects stats that the inventory element can have
	/// </summary>
	public enum InventoryEffectStats { vitality, energy, hunger, thrist, sleep, duration, timeBeforeNext}

	public class InventoryElement
	{
		/// <summary>
		/// Name of the inventory object
		/// </summary>
		public string elementName;
		
		/// <summary>
		/// Sprite to be used
		/// </summary>
		public Sprite elementSprite;

		/// <summary>
		/// Sprite for the inventory
		/// </summary>
		public Sprite inventorySprite;

		/// <summary>
		/// Dictionary with the effects stats that the inventory element can have
		/// </summary>
		public Dictionary<InventoryEffectStats, int> stats;

		/// <summary>
		/// Profession tells who can analize an especific element
		/// -1 All Profesion
		/// </summary>
		public int AnalizeProfesion = -1;

		public string description;

		/// <summary>
		/// The data reference.
		/// </summary>
		public InteractableData dataRef;

		public bool isUsable;

		public bool isRation {
			get{
				return inventorySprite.Equals (dataRef.rationInventorySprite);
			}
		}

		public bool isSpecial {
			get{
				return inventorySprite.Equals (dataRef.specialInventorySprite) || inventorySprite.Equals (dataRef.antidoteSprite);
			}
		}

		public bool isAntidote {
			get { 
				return inventorySprite.Equals (dataRef.antidoteSprite);
			}
		}

		/// <summary>
		/// Creates a random new Inventory Item from a seed
		/// </summary>
		// TODO: Create a json and a reader for precreated elements
		#pragma warning disable 0618
		public void CreateItem(int seed, Dictionary<InventoryEffectStats, int> rangeDictionary = null) {
			elementName = "seed_" + Random.seed;
			if(seed != 0)
				Random.seed = seed;
			stats = new Dictionary<InventoryEffectStats, int>();
			int pos1 = Random.Range(0,4), pos2 = Random.Range(0,4);
			if(pos2 >= pos1) pos2++;
			int val;
			if((pos1 == 0 || pos2 == 0) && rangeDictionary.TryGetValue(InventoryEffectStats.vitality,out val))
				stats.Add(InventoryEffectStats.vitality, Random.Range(-val, val));
			else
				stats.Add(InventoryEffectStats.vitality, 0);

			if ((pos1 == 1 || pos2 == 1) && rangeDictionary.TryGetValue(InventoryEffectStats.energy, out val))
				stats.Add(InventoryEffectStats.energy, Random.Range(1, val));
			else
				stats.Add(InventoryEffectStats.energy, 0);

			if ((pos1 == 2 || pos2 == 2) && rangeDictionary.TryGetValue(InventoryEffectStats.hunger, out val))
				stats.Add(InventoryEffectStats.hunger, Random.Range(1, val));
			else
				stats.Add(InventoryEffectStats.hunger, 0);

			if ((pos1 == 3 || pos2 == 3) && rangeDictionary.TryGetValue(InventoryEffectStats.thrist, out val))
				stats.Add(InventoryEffectStats.thrist, Random.Range(1, val));
			else
				stats.Add(InventoryEffectStats.thrist, 0);

			if ((pos1 == 4 || pos2 == 4) && rangeDictionary.TryGetValue(InventoryEffectStats.sleep, out val))
				stats.Add(InventoryEffectStats.sleep, Random.Range(-val, val));
			else
				stats.Add(InventoryEffectStats.sleep, 0);
			/*if (Random.value < 0.2f && rangeDictionary.TryGetValue(InventoryEffectStats.duration, out val))
			{
				stats.Add(InventoryEffectStats.duration, Random.Range(1, val));
				if(rangeDictionary.TryGetValue(InventoryEffectStats.timeBeforeNext, out val))
					stats.Add(InventoryEffectStats.timeBeforeNext, Random.Range(1, val));
			}
			else
			{*/
			stats.Add(InventoryEffectStats.duration, 0);
			stats.Add(InventoryEffectStats.timeBeforeNext, 0);
			//}
		}

		/// <summary>
		/// Creates an item from an Interactable Data
		/// </summary>
		/// <param name="data"></param>
		public void CreateItem(InteractableData data)
		{
			dataRef = data;
			stats = new Dictionary<InventoryEffectStats, int>();
			stats.Add(InventoryEffectStats.vitality, data.vitality);
			stats.Add(InventoryEffectStats.energy, data.energy);
			stats.Add(InventoryEffectStats.hunger, data.hunger);
			stats.Add(InventoryEffectStats.thrist, data.thrist);
			stats.Add(InventoryEffectStats.sleep, data.sleep);
			stats.Add(InventoryEffectStats.duration, 0);
			stats.Add(InventoryEffectStats.timeBeforeNext, 0);
			isUsable = data.isUsable;
			description = data.description;
		}

		/// <summary>
		/// Apply the effect of the item
		/// </summary>
		void applyEffect(PlayerStatsReduction playerStats)
		{
			int val;
			for (int i = 0; i < 5; i++)
			{
				if (stats.TryGetValue((InventoryEffectStats) i, out val))
					playerStats.consumeStatByAmount(i, -val);
			}
		}

		/// <summary>
		/// Keeps applying the effect over a period of time
		/// </summary>
		/// <param name="playerStats"></param>
		/// <returns></returns>
		public IEnumerator applyEffectOverTime(PlayerStatsReduction playerStats)
		{
			if (Random.value < dataRef.poisonProb) {
				playerStats.consumingTimeStats [0] [0] = dataRef.poisonTime;
				playerStats.poisonLevel = (int) (dataRef.poisonProb * 10);
				playerStats.StartCoroutine(playerStats.consumeStat(0));
			}

			int duration = stats[InventoryEffectStats.duration];
            if (duration == 0)
			{
				applyEffect(playerStats);
			}
			else
			{
				int time = 0;
				while (time < duration)
				{
					applyEffect(playerStats);
					yield return new WaitForSeconds(stats[InventoryEffectStats.timeBeforeNext]);
					time += stats[InventoryEffectStats.timeBeforeNext];
                }
			}

			if (isAntidote) {
				playerStats.poisonLevel -= dataRef.antidoteEfectiveness;
				playerStats.consumingTimeStats [0] [0] += dataRef.antidoteEfectiveness; //Making the effect of the poison weaker
				playerStats.consumeStatByAmount(0, -dataRef.antidoteEfectiveness);
				if (playerStats.poisonLevel <= 0) {
					playerStats.consumingTimeStats [0] [0] = 0;
					playerStats.poisonLevel = 0;
				}
			}
		}
	}

}
