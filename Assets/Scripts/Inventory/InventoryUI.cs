﻿using UnityEngine;
using System.Collections;
using Player;
using UnityEngine.UI;
using Stats;
using SpecialActions.OverInventory;

namespace Inventory
{
	/// <summary>
	/// Handles the interface for the inventory
	/// </summary>
	public class InventoryUI : MonoBehaviour
	{
		/// <summary>
		/// Use this sprite for an empty slot
		/// </summary>
		[SerializeField]
		private Sprite emptySlot;

		/// <summary>
		/// Reference to current player inventory
		/// </summary>
		public static PlayerInventory playerInventory;

		[SerializeField]
		private GameObject[] _inventoryElements;

		/// <summary>
		/// Refence to images in the inventory, must be equal to the size inside the player inventory
		/// </summary>
		[HideInInspector]
		public Image[] _inventoryElementsSprite;

		/// <summary>
		/// Reference to element analysis
		/// </summary>
		[HideInInspector]
		public Image[] _inventoryElementsClock;

		/// <summary>
		/// Reference to element options
		/// </summary>
		[HideInInspector]
		public GameObject[] _inventoryElementOptions;

		/// <summary>
		/// Reference to element analysis
		/// </summary>
		public GameObject _inventoryElementAnalysis;

		private IEnumerator[] countdowns;

		private int currentInventory;

		/// <summary>
		/// Awake this instance
		/// </summary>
		void Awake()
		{
			currentInventory = -1;
			_inventoryElementsSprite = new Image[_inventoryElements.Length];
			_inventoryElementsClock = new Image[_inventoryElements.Length];
			_inventoryElementOptions = new GameObject[_inventoryElements.Length];
			countdowns = new IEnumerator[_inventoryElements.Length];
			for (int i = 0; i < _inventoryElements.Length; i++)
			{
				_inventoryElementsSprite[i] = _inventoryElements[i].transform.GetChild(0).GetComponent<Image>();
				_inventoryElementsClock [i] = _inventoryElementsSprite [i].transform.GetChild (0).GetComponent<Image> ();
				countdowns [i] = Countdown.coutdown (_inventoryElementsClock [i], 0);
				_inventoryElementOptions[i] = _inventoryElements[i].transform.GetChild(1).gameObject;
			}
			Messenger.AddListener(GlobalVariables.MESSAGE_ON_UPDATE_INVENTORY, UpdateInventory);
			Messenger.AddListener<PlayerInventory>(GlobalVariables.MESSAGE_ON_PLAYER_CHANGE_INVENTORY, ChangePlayerInventory);
			Messenger.MarkAsPermanent (GlobalVariables.MESSAGE_ON_UPDATE_INVENTORY);
			Messenger.MarkAsPermanent (GlobalVariables.MESSAGE_ON_PLAYER_CHANGE_INVENTORY);
		}

		void Update(){
			if (currentInventory == -1) {
				if (Input.GetKeyDown (KeyCode.Alpha1) || Input.GetKeyDown (KeyCode.Keypad1)) {
					OpenInventoryOptions (0);
				}
				if (Input.GetKeyDown (KeyCode.Alpha2) || Input.GetKeyDown (KeyCode.Keypad2)) {
					OpenInventoryOptions (1);
				}
				if (Input.GetKeyDown (KeyCode.Alpha3) || Input.GetKeyDown (KeyCode.Keypad3)) {
					OpenInventoryOptions (2);
				}
				if (Input.GetKeyDown (KeyCode.Alpha4) || Input.GetKeyDown (KeyCode.Keypad4)) {
					OpenInventoryOptions (3);
				}
				if (Input.GetKeyDown (KeyCode.Alpha5) || Input.GetKeyDown (KeyCode.Keypad5)) {
					OpenInventoryOptions (4);
				}
				if (Input.GetKeyDown (KeyCode.Alpha6) || Input.GetKeyDown (KeyCode.Keypad6)) {
					OpenInventoryOptions (5);
				}
			} else {
				if ((Input.GetKeyDown (KeyCode.Alpha1) || Input.GetKeyDown (KeyCode.Keypad1)) && _inventoryElementOptions[currentInventory].transform.GetChild(0).GetComponent<Button>().interactable) {
					UseInventoryElement (currentInventory);
				}
				if ((Input.GetKeyDown (KeyCode.Alpha2) || Input.GetKeyDown (KeyCode.Keypad2)) && _inventoryElementOptions[currentInventory].transform.GetChild(1).GetComponent<Button>().interactable)  {
					SpecialActionInventoryElement (currentInventory);
				}
				if ((Input.GetKeyDown (KeyCode.Alpha3) || Input.GetKeyDown (KeyCode.Keypad3)) && _inventoryElementOptions[currentInventory].transform.GetChild(2).GetComponent<Button>().interactable)  {
					DropInventoryElement (currentInventory);
				}
				if ((Input.GetKeyDown (KeyCode.Alpha4) || Input.GetKeyDown (KeyCode.Keypad4)) && _inventoryElementOptions[currentInventory].transform.GetChild(3).GetComponent<Button>().interactable)  {
					CloseAllOptions ();
				}
			}
		}

		void OnDestroy(){
			Messenger.RemoveListener(GlobalVariables.MESSAGE_ON_UPDATE_INVENTORY, UpdateInventory);
			Messenger.RemoveListener<PlayerInventory>(GlobalVariables.MESSAGE_ON_PLAYER_CHANGE_INVENTORY, ChangePlayerInventory);
		}

		/// <summary>
		/// Updates the inventory on screen
		/// </summary>
		public void UpdateInventory()
		{
			//Save timers data
			for (int i = 0; i < _inventoryElementOptions.Length; i++){
				if (countdowns [i].Current != null) {
					playerInventory.timers [i] = (float)countdowns [i].Current;
					playerInventory.StopCoroutine (countdowns [i]);
				}
				_inventoryElementsClock [i].fillAmount = 0;
			}
			bool isAppliable;
			for (int i = 0; i < _inventoryElementsSprite.Length; i++)
			{
				if (playerInventory._inventoryElements[i] != null)
				{
					playerInventory.StopCoroutine (countdowns [i]);
					countdowns [i] = Countdown.coutdown (_inventoryElementsClock [i], playerInventory._inventoryElements[i].dataRef.timer * playerInventory.timerFactor, playerInventory.timers [i]);
					playerInventory.StartCoroutine (countdowns [i]);

					PlayerType[] types = playerInventory._inventoryElements [i].dataRef.playerTypeInventoryActions;
					isAppliable = false;
					if ((!playerInventory._inventoryElements [i].isRation && !playerInventory._inventoryElements [i].isSpecial) || playerInventory.specialAction is Analize) {
						for (int j = 0; j < types.Length && !isAppliable; j++) {
							isAppliable = (types [j] == playerInventory.type || types [j] == PlayerType.all);
						}
					}
					_inventoryElementOptions[i].transform.GetChild(0).GetComponent<Button>().interactable = playerInventory._inventoryElements [i].isUsable;
					_inventoryElementOptions[i].transform.GetChild(1).GetComponent<Button>().interactable = isAppliable;
					_inventoryElementOptions [i].transform.GetChild (1).GetComponentInChildren<Text> ().text = playerInventory.specialActionName;
					_inventoryElementsSprite[i].sprite = playerInventory._inventoryElements[i].inventorySprite;
					_inventoryElementsSprite[i].color = Color.white;

				}
				else
				{
					_inventoryElementsSprite[i].sprite = emptySlot;
					_inventoryElementsSprite[i].color = Color.clear;
				}
			}
		}

		/// <summary>
		/// Use inventory element
		/// </summary>
		/// <param name="index"></param>
		public void UseInventoryElement(int index)
		{

			if(playerInventory.UseInventoryElement(index)){
				StopCoroutine (countdowns [index]);
				countdowns [index] = Countdown.coutdown (_inventoryElementsClock [index], 0);;
			}
			//_inventoryElements[index].sprite = emptySlot;
			_inventoryElementOptions[index].SetActive(false);
			UpdateInventory();

			currentInventory = -1;
        }

		/// <summary>
		/// Special Action
		/// </summary>
		/// <param name="index"></param>
		public void SpecialActionInventoryElement(int index)
		{
			playerInventory.SpecialActionInventoryElement(index, this);

			currentInventory = -1;
		}

		/// <summary>
		/// Drops inventory element
		/// </summary>
		/// <param name="index"></param>
		public void DropInventoryElement(int index)
		{
			if (playerInventory.DropInventoryElement (index)) {
				//_inventoryElements[index].sprite = emptySlot;
				_inventoryElementOptions [index].SetActive (false);
				playerInventory._inventoryElements[index] = null;
				UpdateInventory ();
			}

			currentInventory = -1;
		}

		/// <summary>
		/// Show info for this inventory item
		/// </summary>
		/// <param name="index"></param>
		public void AnalizeInventoryElement(int index)
		{
			string analysis = "Analysis:\n" + playerInventory.AnalizeInventoryElement(index);
			_inventoryElementAnalysis.transform.GetChild(0).GetComponentInChildren<Text>().text = analysis;
			_inventoryElementAnalysis.SetActive(true);
			_inventoryElementOptions[index].SetActive(false);

			currentInventory = -1;
		}

		/// <summary>
		/// Show info for this inventory item
		/// </summary>
		/// <param name="index"></param>
		public void ShowAnalysis(int index, string analysis)
		{
			_inventoryElementAnalysis.transform.GetChild(0).GetComponentInChildren<Text>().text = analysis;
			_inventoryElementAnalysis.SetActive(true);
			_inventoryElementOptions[index].SetActive(false);
		}

		/// <summary>
		/// Show info for this inventory item
		/// </summary>
		/// <param name="index"></param>
		public void CloseAnalysis(int index)
		{
			_inventoryElementAnalysis.SetActive(false);
			_inventoryElementOptions[index].SetActive(true);
		}

		/// <summary>
		/// Change the player
		/// </summary>
		/// <param name="val">current player</param>
		public void ChangePlayerInventory(PlayerInventory val)
		{
			//Save timers data
			for (int i = 0; i < _inventoryElementOptions.Length; i++){
				if (countdowns [i].Current != null) {
					playerInventory.timers [i] = (float)countdowns [i].Current;
					playerInventory.StopCoroutine (countdowns [i]);
					countdowns [i] = Countdown.coutdown (_inventoryElementsClock [i], 0);
				}
				_inventoryElementsClock [i].fillAmount = 0;
			}
			if(playerInventory != null)
				playerInventory.continueTimers ();
			playerInventory = val;
			playerInventory.stopTimers ();
			UpdateInventory();
			for (int i = 0; i < _inventoryElementOptions.Length; i++)
				_inventoryElementOptions[i].SetActive(false);
		}

		/// <summary>
		/// Open inventory Options
		/// </summary>
		/// <param name="index"></param>
		public void OpenInventoryOptions(int index) {
			if (_inventoryElementsSprite [index].transform.GetChild (0).GetComponent<Image> ().fillAmount > 0)
				return;
			currentInventory = index;
			for (int i = 0; i < _inventoryElementOptions.Length; i++)
			{
				_inventoryElementOptions[i].SetActive(false);
				_inventoryElementAnalysis.SetActive(false);
			}
			_inventoryElementOptions[index].SetActive(_inventoryElementsSprite[index].color != Color.clear);
		}

		/// <summary>
		/// Close all inventory options
		/// </summary>
		public void CloseAllOptions()
		{
			currentInventory = -1;
			for (int i = 0; i < _inventoryElementOptions.Length; i++)
			{
				_inventoryElementOptions[i].SetActive(false);
				_inventoryElementAnalysis.SetActive(false);
			}
		}
	}
}
