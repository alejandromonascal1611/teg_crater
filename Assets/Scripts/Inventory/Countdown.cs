﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Countdown {

	public static IEnumerator coutdown(Image clock, float time, float actualTime = -1){
		float timer = actualTime;
		if (actualTime == -1)
			timer = time;
		clock.fillAmount = 1;
		while (timer > 0) {
			yield return timer;
			timer -= Time.deltaTime;
			clock.fillAmount = timer / time;
		}
		clock.fillAmount = 0;
	}

	public static IEnumerator coutdown(float actualTime){
		float timer = actualTime;
		while (timer > 0) {
			yield return timer;
			timer -= Time.deltaTime;
		}
	}
}
