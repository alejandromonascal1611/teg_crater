﻿using UnityEngine;
using Stats;
using System.Collections.Generic;
using Player;
using UnityEngine.SceneManagement;

namespace Managers
{
    /// <summary>
    /// Players Manager
    /// </summary>
    public class PlayersManager : MonoBehaviour
    {
        /// <summary>
        /// Reference to each player
        /// </summary>
        public List<GameObject> playersReference;

        public List<GameObject> staticPlayersReference;

        /// <summary>
        /// The current player
        /// </summary>
        int currentPlayer;

		public GoTo goTo;

        /// <summary>
        /// Init this instance
        /// </summary>
        public void Init()
		{
			currentPlayer = 0;
			playersReference[currentPlayer].SendMessage(GlobalVariables.MESSAGE_ON_PLAYER_TOGGLE, true);
			foreach (GameObject go in playersReference)
			{
				staticPlayersReference.Add(go);
				go.SendMessage(GlobalVariables.MESSAGE_SETUP, this);
			}
			Messenger.Broadcast<Transform>(GlobalVariables.MESSAGE_ON_PLAYER_FOCUS, playersReference[currentPlayer].transform);
			Messenger.Broadcast<PlayerStatsReduction>(GlobalVariables.MESSAGE_ON_PLAYER_CHANGE_STAT, playersReference[currentPlayer].GetComponent<PlayerStatsReduction>());
			Messenger.Broadcast<PlayerInventory>(GlobalVariables.MESSAGE_ON_PLAYER_CHANGE_INVENTORY, playersReference[currentPlayer].GetComponent<PlayerInventory>());
		}

		/// <summary>
		/// Awakes this instance
		/// </summary>
		void Awake()
		{
			Messenger.AddListener<float>(GlobalVariables.MESSAGE_ON_PLAYER_CHANGE, ChangePlayer);
			Messenger.MarkAsPermanent (GlobalVariables.MESSAGE_ON_PLAYER_CHANGE);
		}

		void OnDestroy(){
			Messenger.RemoveListener<float>(GlobalVariables.MESSAGE_ON_PLAYER_CHANGE, ChangePlayer);
		}

		/// <summary>
		/// Change the player
		/// </summary>
		/// <param name="val">Axis raw</param>
		public void ChangePlayer(float val)
		{
            if (Objectives.survivors <= 0) {
                goTo.goToSceneWithTransition(GlobalVariables.badEnding);
                return;
            }
            
			bool flag = false;
			playersReference[currentPlayer].SendMessage(GlobalVariables.MESSAGE_ON_PLAYER_TOGGLE, false);
			while (playersReference[currentPlayer].GetComponent<PlayerStatsReduction>().isDead)
			{
				flag = true;
				playersReference.RemoveAt(currentPlayer);
				if (playersReference.Count == 0)
				{
					goTo.goToSceneWithTransition (GlobalVariables.badEnding);
					return;
				}
				if (currentPlayer == playersReference.Count)
					currentPlayer = 0;
			}

			if(flag) 
				currentPlayer--;
			
			if (val > 0)
            {
				currentPlayer++;
                if (currentPlayer == playersReference.Count)
                    currentPlayer = 0;
            }
            if (val < 0)
            {
                currentPlayer--;
                if (currentPlayer < 0)
                    currentPlayer = playersReference.Count - 1;
            }

            playersReference[currentPlayer].SendMessage(GlobalVariables.MESSAGE_ON_PLAYER_TOGGLE, true);
            Messenger.Broadcast<Transform>(GlobalVariables.MESSAGE_ON_PLAYER_FOCUS, playersReference[currentPlayer].transform);
            Messenger.Broadcast<PlayerStatsReduction>(GlobalVariables.MESSAGE_ON_PLAYER_CHANGE_STAT, playersReference[currentPlayer].GetComponent<PlayerStatsReduction>());
			Messenger.Broadcast<PlayerInventory>(GlobalVariables.MESSAGE_ON_PLAYER_CHANGE_INVENTORY, playersReference[currentPlayer].GetComponent<PlayerInventory>());
		}
    }
}
