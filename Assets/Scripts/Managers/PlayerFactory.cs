﻿using UnityEngine;
using SimpleJSON;
using Stats;
using Player;

namespace Managers
{
	/// <summary>
	/// Creates the players on the scene
	/// </summary>
	[RequireComponent(typeof(PlayersManager))]
	public class PlayerFactory : MonoBehaviour
	{

		/// <summary>
		/// Player Prefab Reference
		/// </summary>
		public GameObject playerPrefab;

		/// <summary>
		/// Position to be used as target
		/// </summary>
		public Transform target;

		public Transform cam;

		public PlayerData passiveConsumingTime;
		public PlayerData activeConsumingTime;

		public PlayerData[] playersData;

		/// <summary>
		/// Starts this instance
		/// </summary>
		void Start()
		{
			//JSONNode jsonData = JSONNode.Parse(Resources.Load<TextAsset>("Data/ProfessionStats").text);
			//JSONArray consumingTimeData = jsonData["ConsumingTime"].AsArray;
			//JSONArray professionsData = jsonData["PlayerStats"].AsArray;
			//GameObject temp;
			//PlayersManager playerManager = GetComponent<PlayersManager>();
			//PlayerStats[] consumingTimeStats = new PlayerStats[2];
			//consumingTimeStats[0]= new PlayerStats();
			//consumingTimeStats[1] = new PlayerStats();
			//consumingTimeStats[0].Deserialize(consumingTimeData[0]);
			//consumingTimeStats[1].Deserialize(consumingTimeData[1]);
			//int count;
			//for (int i = 0; i < professionsData.Count; i++) {
			//	count = 0;
			//	while (Physics.OverlapSphere(target.position + new Vector3(i * 2, count), 0.5f).Length > 0)
			//		count++;
			//	while (Physics.OverlapSphere(target.position + new Vector3(i * 2, count - 1), 0.5f).Length == 0)
			//		count--;

			//	temp = (GameObject) Instantiate(playerPrefab, target.position + new Vector3(i * 2, count), Quaternion.identity);
			//	temp.GetComponent<PlayerStatsReduction>().Init(professionsData[i], false, consumingTimeStats);
			//	//temp.GetComponent<PlayerStatsReduction>().Init(professionsData[i], true);
			//	temp.name = professionsData[i]["name"];
			//	playerManager.playersReference.Add(temp);
			//	temp.SendMessage(GlobalVariables.MESSAGE_ON_PLAYER_TOGGLE, false);
			//}

			PlayersManager playerManager = GetComponent<PlayersManager>();
			int count = 0;
			GameObject temp;

			for (int i = 0; i < playersData.Length; i++)
			{
				count = 0;
				while (Physics.OverlapSphere(target.position + new Vector3(i * 2, count), 0.5f).Length > 0)
					count++;
				while (Physics.OverlapSphere(target.position + new Vector3(i * 2, count - 1), 0.5f).Length == 0 && Mathf.Abs(count + target.position.y) <= 30)
					count--;
				count--;

				temp = (GameObject)Instantiate(playerPrefab, target.position + new Vector3(i * 2, count), Quaternion.identity);
				temp.GetComponent<PlayerStatsReduction>().Init(playersData[i], false, passiveConsumingTime, activeConsumingTime);
				PlayerInventory inventory = temp.GetComponent<PlayerInventory> ();
				inventory.specialActionName = playersData [i].specialActionName;
				inventory.specialAction = SpecialActionsHandler.Instance.specialActionsReferences [playersData [i].specialActionID];
				inventory.type = playersData [i].type;
				inventory.timerFactor = playersData [i].timerFactor;
				temp.name = playersData[i].uiName;
				playerManager.playersReference.Add(temp);
				temp.SendMessage(GlobalVariables.MESSAGE_ON_PLAYER_TOGGLE, false);
				temp.GetComponent<Animator>().runtimeAnimatorController = playersData[i].animator;
			}

			cam.position = playerManager.playersReference[0].transform.position;

			playerManager.Init();
		}
	}
}
