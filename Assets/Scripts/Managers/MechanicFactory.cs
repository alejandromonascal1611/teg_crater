﻿using UnityEngine;
using SimpleJSON;
using Stats;
using Player;

namespace Managers
{
	/// <summary>
	/// Creates the players on the scene
	/// </summary>
	public class MechanicFactory : MonoBehaviour
	{

		public GameObject spaceShip;

		public Transform target;

		[SerializeField]
		public LayerMask _floorLayers;

		/// <summary>
		/// Starts this instance
		/// </summary>
		void Start()
		{
			int count = 0;
			while (Physics.OverlapSphere(target.position + new Vector3(0, count), 0.5f, _floorLayers).Length > 0)
				count++;
			while (Physics.OverlapSphere(target.position + new Vector3(0, count - 1), 0.5f, _floorLayers).Length == 0 && Mathf.Abs(count + target.position.y) <= 30)
				count--;
			count--;

			Instantiate(spaceShip, target.position + new Vector3(0, count), Quaternion.identity);
		}

	}
}
