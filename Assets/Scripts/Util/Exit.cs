﻿using UnityEngine;
using System.Collections;

public class Exit : MonoBehaviour {
	
	/// <summary>
	/// Updates this instance
	/// </summary>
	void Update () {
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			exit();
		}
	}

	public void exit(){
		Application.Quit();
	}
}
