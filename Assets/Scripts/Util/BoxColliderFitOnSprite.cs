﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
[RequireComponent(typeof(BoxCollider))]
public class BoxColliderFitOnSprite : MonoBehaviour {
	
	void Update () {
		Vector3 S = gameObject.GetComponent<SpriteRenderer>().sprite.bounds.size;
		gameObject.GetComponent<BoxCollider>().size = S;
		gameObject.GetComponent<BoxCollider>().center = new Vector3(0, S.y * 0.5f, 0);
	}
}
