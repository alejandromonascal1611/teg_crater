﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ReceiveMessage : MonoBehaviour {

	[SerializeField]
	private Text text;

	private CanvasGroup canvasGroup;

	/// <summary>
	/// Awakes this instance
	/// </summary>
	void Awake()
	{
		canvasGroup = GetComponent<CanvasGroup> ();
		canvasGroup.alpha = 0;
		Messenger.AddListener<string>(GlobalVariables.MESSAGE_ON_RECEIVE_MESSAGE, ReceiveText);
		Messenger.MarkAsPermanent (GlobalVariables.MESSAGE_ON_RECEIVE_MESSAGE);
	}

	/// <summary>
	/// Raises the destroy event.
	/// </summary>
	void OnDestroy(){
		Messenger.RemoveListener<string>(GlobalVariables.MESSAGE_ON_RECEIVE_MESSAGE, ReceiveText);
	}
	
	/// <summary>
	/// Receives the text.
	/// </summary>
	/// <param name="txt">Text.</param>
	void ReceiveText (string txt) {
		canvasGroup.alpha = 1;
		text.text = txt;
		StartCoroutine (Shootdown (txt.Length * 0.3f));
	}

	IEnumerator Shootdown(float time){
		yield return new WaitForSecondsRealtime (time);
		canvasGroup.alpha = 0;
	}
}
