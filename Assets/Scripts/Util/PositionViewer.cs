﻿using Managers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PositionViewer : MonoBehaviour {

	public PlayersManager playersManager;
	Transform mainCamera;
	public GameObject[] dirLeft;
	public GameObject[] dirRight;
	public float minShowDistance = 5;

	private void Start()
	{
		mainCamera = UnityEngine.Camera.main.transform;
	}
	
	void Update () {
		int pos;
		for(int i=0; i< dirLeft.Length; i++)
		{
			pos = Mathf.CeilToInt(playersManager.staticPlayersReference[i].transform.position.x - mainCamera.position.x);
			//Left Side
			if (pos < -minShowDistance)
			{
				dirLeft[i].SetActive(true);
				dirRight[i].SetActive(false);
				dirLeft[i].GetComponentInChildren<Text>().text = Mathf.Abs(pos) + " mts";
			}
			else if (pos > minShowDistance)
			{
				dirRight[i].SetActive(true);
				dirLeft[i].SetActive(false);
				dirRight[i].GetComponentInChildren<Text>().text = Mathf.Abs(pos) + " mts";
			}
			else
			{
				dirRight[i].SetActive(false);
				dirLeft[i].SetActive(false);
			}
		}
	}
}
