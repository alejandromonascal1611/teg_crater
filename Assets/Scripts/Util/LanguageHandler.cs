﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LanguageHandler : MonoBehaviour {

	[SerializeField]
	private LanguagePair[] languagePairs;

	public static SystemLanguage currentLanguage = SystemLanguage.Spanish;

	// Use this for initialization
	void Start () {
//		int i;
		foreach(LanguagePair lp in languagePairs){
			lp.uiReference.text = lp.textData [currentLanguage];
		}
	}
}

[System.Serializable]
public struct LanguagePair {
	public LanguageData textData;
	public Text uiReference;
}
