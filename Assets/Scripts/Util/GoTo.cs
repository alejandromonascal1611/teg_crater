﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GoTo : MonoBehaviour {

	public static int previousScene;
	
	public void goToScene(int sceneIndex)
	{
		previousScene = SceneManager.GetActiveScene().buildIndex;
		SceneManager.LoadScene(sceneIndex);
	}

	public void goToScene(string sceneName)
	{
		previousScene = SceneManager.GetActiveScene().buildIndex;
		SceneManager.LoadScene(sceneName);
	}

	public void goToPreviousScene()
	{
		SceneManager.LoadScene(previousScene);
	}

	public void goToSceneWithTransition(string sceneName){
		GetComponent<Animator> ().SetTrigger ("Exit");
		StartCoroutine(waitToGo(sceneName, GetComponent<Animator>().GetCurrentAnimatorClipInfo(0)[0].clip.length));
	}

	public void goToSceneWithTransition(int sceneIndex){
		GetComponent<Animator> ().SetTrigger ("Exit");
		StartCoroutine(waitToGo(sceneIndex, GetComponent<Animator>().GetCurrentAnimatorClipInfo(0)[0].clip.length));
	}

	private IEnumerator waitToGo(string sceneName, float time){
		yield return new WaitForSeconds (time);
		goToScene (sceneName);
	}

	private IEnumerator waitToGo(int sceneIndex, float time){
		yield return new WaitForSeconds (time);
		goToScene (sceneIndex);
	}
}
