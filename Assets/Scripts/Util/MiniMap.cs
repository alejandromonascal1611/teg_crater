﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniMap : MonoBehaviour {

	public UnityEngine.Camera miniMapCamera;
	public GameObject minimapPanel;
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Tab))
		{
			miniMapCamera.gameObject.SetActive(true);
			minimapPanel.SetActive(true);
		}
		else if (Input.GetKeyUp(KeyCode.Tab))
		{
			miniMapCamera.gameObject.SetActive(false);
			minimapPanel.SetActive(false);
		}
	}
}
