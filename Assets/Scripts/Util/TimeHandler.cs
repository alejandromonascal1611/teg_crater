﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TimeHandler : MonoBehaviour {

	/// <summary>
	/// The max speed.
	/// If maxSpeed <= 0, then ignore maxSpeed
	/// </summary>
	public float maxSpeed = 8;

	/// <summary>
	/// The min speed.
	/// If minSpeed <= 0, then ignore minSpeed
	/// </summary>
	public float minSpeed = 0.125f;

	public float speedMultiplier = 2;

	private float _inverseSpeed;

	void Awake(){
		_inverseSpeed = 1f / speedMultiplier;
	}

	public void Pause () {
		Time.timeScale = 0;
	}

	public void Play () {
		Time.timeScale = 1;
	}

	public void Fast () {
		Time.timeScale *= speedMultiplier;
		if (maxSpeed > 0 && Time.timeScale > maxSpeed)
			Time.timeScale = maxSpeed;
	}

	public void Slow () {
		Time.timeScale *= _inverseSpeed;
		if (minSpeed > 0 && Time.timeScale < minSpeed)
			Time.timeScale = minSpeed;
	}

	public void updateText(Text t){
		t.text = "Time: x" + Time.timeScale;
	}
}
