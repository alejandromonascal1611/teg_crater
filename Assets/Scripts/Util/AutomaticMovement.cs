﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class AutomaticMovement : MonoBehaviour {

	public bool loop;
	public bool resetPosition;
	public Vector3[] positions;
	public float time = 10;

	private int i= 0;
	private float speed;

	public float treshHold = 0.1f;

	public UnityEvent OnEndMovement;
	public UnityEvent OnEachStep;

	void Start(){
		speed = 1f / time;
		StartCoroutine (MyUpdate());
	}

	// Update is called once per frame
	IEnumerator MyUpdate () {
		Vector3 dist = (positions [0] - transform.localPosition).normalized;
		while(i < positions.Length){
			transform.localPosition += Time.deltaTime * speed * dist;
			if ((transform.localPosition - positions [i]).magnitude < treshHold) {
				i++;
				if (i == positions.Length) {
					if (loop)
						i = 0;
					if (resetPosition) {
						transform.localPosition = positions [0];
					}
				}
				if (i != positions.Length)
					dist = (positions [i] - transform.localPosition).normalized;
				if (OnEachStep != null)
					OnEachStep.Invoke ();
			}
			yield return 0;
		}
		if(OnEndMovement != null)
			OnEndMovement.Invoke ();
	}
}
