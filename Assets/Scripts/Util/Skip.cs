﻿using UnityEngine;
using System.Collections;

public class Skip : MonoBehaviour {

	public GoTo goTo;
	public string sceneName;

	/// <summary>
	/// Updates this instance
	/// </summary>
	void Update () {
		if (Input.GetKeyDown(KeyCode.Space))
		{
			goTo.goToSceneWithTransition (sceneName);
		}
	}
}
