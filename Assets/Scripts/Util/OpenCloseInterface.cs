﻿using UnityEngine;

public class OpenCloseInterface : MonoBehaviour {

	[SerializeField]
	private GameObject _UIObject;

	[SerializeField]
	private bool startActive;

	[SerializeField]
	private KeyCode key;

	/// <summary>
	/// Starts this instance
	/// </summary>
	void Start () {
		_UIObject.SetActive(startActive);
	}
	
	/// <summary>
	/// Trigger this instance
	/// </summary>
	public void Trigger () {
		_UIObject.SetActive(!_UIObject.activeSelf);
	}
}
