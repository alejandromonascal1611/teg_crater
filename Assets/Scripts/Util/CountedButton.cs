﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class CountedButton : MonoBehaviour {

	public int limit;

	public UnityEvent onCountEnded;

	private int count = 0;

	public void OnClick () {
		count++;
		if(count == limit){
			if (onCountEnded != null)
				onCountEnded.Invoke ();
			count = 0;
		}
	}
}
