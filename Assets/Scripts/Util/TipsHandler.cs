﻿using UnityEngine;
using System.Collections;

public class TipsHandler : MonoBehaviour {
	
	[SerializeField]
	private float tipUpdateTime;

	[SerializeField]
	[Range(0,2)]
	private float tipUpdateProgression = 1;

	[SerializeField]
	[TextArea]
	private string[] tips;

	// Use this for initialization
	void Start () {
		StartCoroutine (MyUpdate());
	}
	
	// Update is called once per frame
	IEnumerator MyUpdate () {
		while (true) {
			yield return new WaitForSecondsRealtime (tipUpdateTime);
			tipUpdateTime *= tipUpdateProgression;
			Messenger.Broadcast<string> (GlobalVariables.MESSAGE_ON_RECEIVE_MESSAGE, tips [Random.Range (0, tips.Length)]);
		}
	}
}
