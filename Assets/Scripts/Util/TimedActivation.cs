﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class TimedActivation : MonoBehaviour {

	[SerializeField]
	private UnityEvent events;

	public void Activate(float time){
		StartCoroutine (ActivateOverTime (time));
	}

	// Use this for initialization
	public IEnumerator ActivateOverTime (float time) {
		yield return new WaitForSeconds (time);
		if(events != null)
			events.Invoke ();
	}
}
