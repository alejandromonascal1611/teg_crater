﻿using UnityEngine;
using Stats;

[CreateAssetMenu(fileName = "InteractableData", menuName = "Game Data/Interactable Data", order = 1)]
public class InteractableData : ScriptableObject
{

	[Header("Visuals")]
	public Sprite sprite;
	public Sprite inventorySprite;
	public Sprite rationSprite;
	public Sprite rationInventorySprite;
	public Sprite specialSprite;
	public Sprite specialInventorySprite;

	[Header("Stats")]
	public int vitality;
	public int energy;
	public int hunger;
	public int thrist;
	public int sleep;

	[Header("SpecialStats")]
	public int spVitality;
	public int spEnergy;
	public int spHunger;
	public int spThrist;
	public int spSleep;

	[Header("Poison and Antidote")]
	[Range(0,1)]
	public float poisonProb;
	public int poisonTime;
	public Sprite antidoteSprite;
	public int antidoteEfectiveness;

	[Header("Procedural Generation Data")]
	public int maxSpread;

	[Header("Special Actions")]
	public PlayerType[] playerTypeInventoryActions;
	public float timer;
	public bool isUsable = true;

	[Header("Description")]
	[TextArea]
	public string description;
	[TextArea]
	public string rationDescription;
	[TextArea]
	public string specialDescription;
	[TextArea]
	public string antidoteDescription;

}
