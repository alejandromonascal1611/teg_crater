﻿using UnityEngine;

[CreateAssetMenu(fileName = "ManualData", menuName = "Game Data/Manual Data", order = 2)]
public class ManualData : ScriptableObject {

	[TextArea]
	public string description;
	public Sprite sprite;

}
