﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "LanguageData", menuName = "Game Data/Language Data", order = 3)]
public class LanguageData : ScriptableObject {
	[SerializeField]
	private LanguageText[] _languageTexts;

	private Dictionary<SystemLanguage,string> _dictionary;

	public string this[SystemLanguage key]{
		get{
			if (_dictionary == null) {
				_dictionary = new Dictionary<SystemLanguage, string> ();
				foreach (LanguageText lt in _languageTexts) 
					_dictionary.Add (lt.language, lt.text);
			}
			return _dictionary [key];
		}
	}
}

[System.Serializable]
public struct LanguageText {
	public SystemLanguage language;
	[TextArea]
	public string text;
}