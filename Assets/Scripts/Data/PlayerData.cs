﻿using UnityEngine;
using Stats;
using UnityEngine.Events;

[CreateAssetMenu(fileName = "PlayerData", menuName = "Game Data/Player Data", order = 0)]
public class PlayerData : ScriptableObject {

	public string uiName;
	public PlayerType type;
	public int vitality;
	public int vitalityVariation;
	public int energy;
	public int energyVariation;
	public int hunger;
	public int hungerVariation;
	public int thrist;
	public int thristVariation;
	public int sleep;
	public int sleepVariation;
	public int habilityConsumption;
	public float timerFactor;
	public AnimatorOverrideController animator;
	public string specialActionName;
	public int specialActionID;

	public PlayerStats createPlayerStats()
	{
		PlayerStats ret = new PlayerStats();
		ret.stats.Add(StatType.vitality, vitality + Random.Range(-vitalityVariation, vitalityVariation));
		ret.stats.Add(StatType.energy, energy + Random.Range(-energyVariation, energyVariation));
		ret.stats.Add(StatType.hunger, hunger + Random.Range(-hungerVariation, hungerVariation));
		ret.stats.Add(StatType.thrist, thrist + Random.Range(-thristVariation, thristVariation));
		ret.stats.Add(StatType.sleep, sleep + Random.Range(-sleepVariation, sleepVariation));
		ret.stats.Add(StatType.habilityConsumption, habilityConsumption);

		return ret;
	}
}
