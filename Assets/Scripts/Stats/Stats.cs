﻿using System.Collections.Generic;
using SimpleJSON;

namespace Stats
{
	/// <summary>
	/// Defines the stats that the player can have
	/// </summary>
	public enum StatType { vitality, energy, hunger, thrist, sleep, habilityConsumption }

	/// <summary>
	/// The type of the player, used for the special ability
	/// </summary>
	public enum PlayerType { farmer, cook, doctor, student, explorer, mechanic, none, all}

	/// <summary>
	/// Handles the stats of the player
	/// </summary>
	[System.Serializable]
	public class PlayerStats : ISerializable
	{
		/// <summary>
		/// Dictionary with the stats of the player
		/// </summary>
		public Dictionary<StatType, int> stats;

		public string name;

		/// <summary>
		/// Constructor
		/// </summary>
		public PlayerStats() {
			stats = new Dictionary<StatType, int>();
		}

		/// <summary>
		/// Copy other player stats
		/// </summary>
		public void NewCopyFrom(Dictionary<StatType, int> original)
		{
			stats = new Dictionary<StatType, int>();
			int val;
			for (int i = 0; i < original.Count; i++)
			{
				if (original.TryGetValue((StatType)i, out val))
					stats.Add((StatType)i, val);
            }
		}

		/// <summary>
		/// Serialize
		/// </summary>
		/// <returns>Data serialized</returns>
		public JSONNode Serialize()
		{
			JSONNode jsonData = new JSONClass();
			int val;
			jsonData.Add("name", name);
			for (int i = 0; i < stats.Count; i++) {
				if(stats.TryGetValue((StatType)i, out val))
	                jsonData.Add(((StatType)i).ToString(), new JSONData(val));
			}
			return jsonData;
		}

		/// <summary>
		/// Deserialize
		/// </summary>
		/// <param name="jsonData">Data to be deserialized</param>
		public void Deserialize(JSONNode jsonData)
		{
			name = jsonData["name"];
            int variance = jsonData["vitalityVariation"].AsInt;
            stats.Add(StatType.vitality, jsonData["vitality"].AsInt + UnityEngine.Random.Range(-variance,variance));

			variance = jsonData["energyVariation"].AsInt;
			stats.Add(StatType.energy, jsonData["energy"].AsInt + UnityEngine.Random.Range(-variance, variance));

			variance = jsonData["hungerVariation"].AsInt;
			stats.Add(StatType.hunger, jsonData["hunger"].AsInt + UnityEngine.Random.Range(-variance, variance));

			variance = jsonData["thristVariation"].AsInt;
			stats.Add(StatType.thrist, jsonData["thrist"].AsInt + UnityEngine.Random.Range(-variance, variance));

			variance = jsonData["sleepVariation"].AsInt;
			stats.Add(StatType.sleep, jsonData["sleep"].AsInt + UnityEngine.Random.Range(-variance, variance));

			stats.Add(StatType.habilityConsumption, jsonData["habilityConsumption"].AsInt);
		}

		/// <summary>
		/// Easy access to the dictionary
		/// </summary>
		public int this[int key]
		{
			get
			{
				return stats[(StatType) key];
			}
			set
			{
				stats[(StatType) key] = value;
			}
		}

		/// <summary>
		/// Easy access to the dictionary
		/// </summary>
		public int this[StatType key]
		{
			get
			{
				return stats[key];
			}
			set
			{
				stats[key] = value;
			}
		}

		/// <summary>
		/// Easy access to the dictionary size
		/// </summary>
		public int size {
			get
			{
				return stats.Count;
			}
		}
	}
}
