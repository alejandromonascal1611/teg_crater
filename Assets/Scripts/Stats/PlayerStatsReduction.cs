﻿using UnityEngine;
using System.Collections;
using SimpleJSON;
using ProceduralGeneration;

namespace Stats
{
	/// <summary>
	/// Handles the individual player Stats Reduction over time
	/// </summary>
	public class PlayerStatsReduction : MonoBehaviour
	{
		public PlayerType playerType;

		/// <summary>
		/// The Default Player Stats
		/// </summary>
		public PlayerStats defaultPlayerStats;

		/// <summary>
		/// The Consuming Time for each stat
		/// [0] passive consuming
		/// [1] active consuming
		/// </summary>
		public PlayerStats[] consumingTimeStats;

		/// <summary>
		/// The real player stat
		/// </summary>
		[HideInInspector]
		public PlayerStats consumedStats;

		/// <summary>
		/// Is the player dead
		/// </summary>
		public bool isDead;

		/// <summary>
		/// Is this the current player
		/// </summary>
		public bool _isCurrentPlayer;

		public int poisonLevel;

		/// <summary>
		/// Initialize this instance
		/// </summary>
		/// <param name="jsonData">Data to be updated</param>
		/// <param name="isUserData">Checks if it is user data or not</param>
		public void Init(JSONNode jsonData, bool isUserData, PlayerStats[] consumingTimeStats = null)
		{
			if (consumingTimeStats != null)
				this.consumingTimeStats = consumingTimeStats;

			if (isUserData)
				consumedStats.Deserialize(jsonData);
			else
			{
				defaultPlayerStats.Deserialize(jsonData);
				consumedStats.NewCopyFrom(defaultPlayerStats.stats);
			}
		}

		/// <summary>
		/// Initialize this instance
		/// </summary>
		public void Init(PlayerData playerData, bool isUserData, PlayerData passiveConsumingTimeStats, PlayerData activeConsumingTimeStats, PlayerData consumedData = null)
		{
			playerType = playerData.type;

			if (consumingTimeStats != null)
			{
				consumingTimeStats = new PlayerStats[2];
				consumingTimeStats[0] = passiveConsumingTimeStats.createPlayerStats();
				consumingTimeStats[1] = activeConsumingTimeStats.createPlayerStats();
			}

			defaultPlayerStats = playerData.createPlayerStats();
			if (consumedData != null)
				consumedStats = consumedData.createPlayerStats();
			else
				consumedStats.NewCopyFrom(defaultPlayerStats.stats);
		}

		/// <summary>
		/// Starts this instance
		/// </summary>
		void Start()
		{
			for (int i = 0; i < defaultPlayerStats.size; i++)
				StartCoroutine(consumeStat(i));
		}

		/// <summary>
		/// Consume an stat by its index
		/// </summary>
		/// <param name="statIndex"> The index of the stat</param>
		/// <returns>yield coroutine</returns>
		public IEnumerator consumeStat(int statIndex)
		{
			float actingMultiplier;
			while (consumingTimeStats[0][statIndex] > 0)
			{
				//if (_isCurrentPlayer)
				//{
					actingMultiplier = consumingTimeStats[1][statIndex] * 0.01f;
				//}
				//else
				//	actingMultiplier = 1;
				yield return new WaitForSeconds(consumingTimeStats[0][statIndex] * actingMultiplier);
				consumedStats[statIndex]--;
				if (consumedStats[statIndex] <= 0)
				{
					consumedStats [statIndex] = 0;

					KillPlayer ();

					if (_isCurrentPlayer)
						Messenger.Broadcast<float>(GlobalVariables.MESSAGE_ON_PLAYER_CHANGE, 1);
					break;
				}
			}
		}

		/// <summary>
		/// Consume an stat by the index
		/// </summary>
		/// <param name="statIndex">the stat index</param>
		/// <param name="amount">the amount to consume</param>
		public void consumeStatByAmount(int statIndex, int amount)
		{
			consumedStats[statIndex] -= amount;
			if (consumedStats[statIndex] > defaultPlayerStats[statIndex])
				consumedStats[statIndex] = defaultPlayerStats[statIndex];
			if (consumedStats[statIndex] <= 0)
			{
				consumedStats [statIndex] = 0;

				KillPlayer ();

				if (_isCurrentPlayer)
					Messenger.Broadcast<int>(GlobalVariables.MESSAGE_ON_PLAYER_CHANGE, 1);
			}
		}

		/// <summary>
		/// Sets if the player is active or not
		/// </summary>
		/// <param name="isActive">the active toggle</param>
		void OnPlayerToggle(bool isActive)
		{
			_isCurrentPlayer = isActive;
		}

		void KillPlayer(){
			isDead = true;
			Objectives.survivors--;

			GetComponent<Animator> ().SetTrigger ("IsDead");

			InteractablesGenerator refIG = FindObjectOfType<InteractablesGenerator>();
			LayerMask _floorLayers = refIG._floorLayers;
			Vector3 position = new Vector3(Mathf.Round(transform.position.x)-0.5f,Mathf.Round(transform.position.y),transform.position.z);
			Vector3 size = new Vector3(GetComponent<Renderer>().bounds.size.x * 0.5f, GetComponent<Renderer>().bounds.size.y * 0.5f, GetComponent<Renderer>().bounds.size.z * 0.5f);
			int count = 0;
			while (Physics.OverlapBox(position + new Vector3(0, count), size, transform.rotation, _floorLayers).Length > 0)
				count++;
			while (Physics.OverlapBox(position + new Vector3(0, count - 1), size, transform.rotation, _floorLayers).Length == 0)
				count--;
			count--;

			transform.position = position + new Vector3(0, count);
		}

	}
}