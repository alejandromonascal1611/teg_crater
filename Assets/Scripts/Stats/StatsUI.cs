﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Stats
{
	/// <summary>
	/// Stats User Interface
	/// </summary>
    public class StatsUI : MonoBehaviour
    {
		/// <summary>
		/// Time for each stat update
		/// </summary>
		[SerializeField]
		private float _updateTime;

		/// <summary>
		/// Reference to UI sliders
		/// </summary>
		[SerializeField]
		private Slider[] _statsSliders;

		/// <summary>
		/// Reference to UI texts
		/// </summary>
		[SerializeField]
		private Text[] _statsTexts;

		/// <summary>
		/// Reference to UI player name on stats
		/// </summary>
		[SerializeField]
		private Text _name;

		/// <summary>
		/// Reference to a player stats
		/// </summary>
		private PlayerStatsReduction _playerStatsReference;
		
		/// <summary>
		/// Awake this instance
		/// </summary>
		void Awake()
        {
			Messenger.AddListener<PlayerStatsReduction>(GlobalVariables.MESSAGE_ON_PLAYER_CHANGE_STAT, ChangePlayer);
			Messenger.MarkAsPermanent (GlobalVariables.MESSAGE_ON_PLAYER_CHANGE_STAT);
        }

		void OnDestroy(){
			Messenger.RemoveListener<PlayerStatsReduction>(GlobalVariables.MESSAGE_ON_PLAYER_CHANGE_STAT, ChangePlayer);
			StopAllCoroutines ();
		}

		void Start() {
			StartCoroutine(OnShowStats());
		}

        /// <summary>
        /// Shows Information of current stat
        /// </summary>
        IEnumerator OnShowStats()
        {
			while (true) {
				ShowStat();
				yield return new WaitForSeconds(_updateTime);
			}
		}

		void ShowStat() {
			if (_playerStatsReference == null)
				return;
				
			//Set the slider values
			_statsSliders[0].value = _playerStatsReference.consumedStats[StatType.vitality] / (float)_playerStatsReference.defaultPlayerStats[StatType.vitality];
			_statsSliders[1].value = _playerStatsReference.consumedStats[StatType.energy] / (float)_playerStatsReference.defaultPlayerStats[StatType.energy];
			_statsSliders[2].value = _playerStatsReference.consumedStats[StatType.hunger] / (float)_playerStatsReference.defaultPlayerStats[StatType.hunger];
			_statsSliders[3].value = _playerStatsReference.consumedStats[StatType.thrist] / (float)_playerStatsReference.defaultPlayerStats[StatType.thrist];
			_statsSliders[4].value = _playerStatsReference.consumedStats[StatType.sleep] / (float)_playerStatsReference.defaultPlayerStats[StatType.sleep];

			//Set the text values
			_statsTexts[0].text = _playerStatsReference.consumedStats[StatType.vitality] + "/" + _playerStatsReference.defaultPlayerStats[StatType.vitality];
			_statsTexts[1].text = _playerStatsReference.consumedStats[StatType.energy] + "/" + _playerStatsReference.defaultPlayerStats[StatType.energy];
			_statsTexts[2].text = _playerStatsReference.consumedStats[StatType.hunger] + "/" + _playerStatsReference.defaultPlayerStats[StatType.hunger];
			_statsTexts[3].text = _playerStatsReference.consumedStats[StatType.thrist] + "/" + _playerStatsReference.defaultPlayerStats[StatType.thrist];
			_statsTexts[4].text = _playerStatsReference.consumedStats[StatType.sleep] + "/" + _playerStatsReference.defaultPlayerStats[StatType.sleep];
		}

		/// <summary>
		/// Change the player
		/// </summary>
		/// <param name="val">current player</param>
		public void ChangePlayer(PlayerStatsReduction val)
		{
			_playerStatsReference = val;
			_name.text = _playerStatsReference.name + ":";
			ShowStat();
        }
    }
}