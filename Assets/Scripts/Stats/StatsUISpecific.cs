﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using Managers;

namespace Stats
{
	/// <summary>
	/// Stats User Interface
	/// </summary>
    public class StatsUISpecific : MonoBehaviour
    {
		[SerializeField]
		private int _playerStatIndex;

		/// <summary>
		/// Time for each stat update
		/// </summary>
		[SerializeField]
		private float _updateTime;

		/// <summary>
		/// Reference to UI sliders
		/// </summary>
		[SerializeField]
		private Slider[] _statsSliders;

		/// <summary>
		/// Reference to UI player name on stats
		/// </summary>
		[SerializeField]
		private Text _name;

		[SerializeField]
		private PlayersManager _playerManager;

		[Header("Color Help")]
		[SerializeField]
		private Color _normalColor;
		[SerializeField]
		private Color _activeColor;
		[SerializeField]
		private Color _poisonColor;
		[SerializeField]
		private Color _dangerColor1;
		[SerializeField]
		private Color _dangerColor2;
		[SerializeField]
		private Color _deadColor;

		private Color _actualColor;

		[SerializeField]
		[Range(0,1)]
		private float _danger1;

		[SerializeField]
		[Range(0,1)]
		private float _danger2;

		[SerializeField]
		private Image backPanel;

		[SerializeField]
		private Image _playerImg;

		[SerializeField]
		private Sprite _deadSprite;

		/// <summary>
		/// Reference to a player stats
		/// </summary>
		private PlayerStatsReduction _playerStatsReference;

		/// <summary>
		/// Awake this instance
		/// </summary>
		void Awake()
		{
			Messenger.AddListener<PlayerStatsReduction>(GlobalVariables.MESSAGE_ON_PLAYER_CHANGE_STAT, ChangePlayer);
			Messenger.MarkAsPermanent (GlobalVariables.MESSAGE_ON_PLAYER_CHANGE_STAT);
		}

		void OnDestroy(){
			Messenger.RemoveListener<PlayerStatsReduction>(GlobalVariables.MESSAGE_ON_PLAYER_CHANGE_STAT, ChangePlayer);
			StopAllCoroutines ();
		}

		void Start() {
			if (_playerStatIndex == 0) {
				backPanel.color = _activeColor;
				_actualColor = _activeColor;
			}
			StartCoroutine(OnShowStats());
		}

        /// <summary>
        /// Shows Information of current stat
        /// </summary>
        IEnumerator OnShowStats()
        {
			while (true) {
				ShowStat();
				yield return new WaitForSeconds(_updateTime);
			}
		}

		void ShowStat() {
			if (_playerManager.playersReference.Count == 0)
				return;
			if (_playerStatsReference == null) {
				_playerStatsReference = _playerManager.playersReference [_playerStatIndex].GetComponent<PlayerStatsReduction>();
				StartCoroutine(Poisoned());
			}

			//Set the slider values
			_statsSliders[0].value = _playerStatsReference.consumedStats[StatType.vitality] / (float)_playerStatsReference.defaultPlayerStats[StatType.vitality];
			_statsSliders[1].value = _playerStatsReference.consumedStats[StatType.energy] / (float)_playerStatsReference.defaultPlayerStats[StatType.energy];
			_statsSliders[2].value = _playerStatsReference.consumedStats[StatType.hunger] / (float)_playerStatsReference.defaultPlayerStats[StatType.hunger];
			_statsSliders[3].value = _playerStatsReference.consumedStats[StatType.thrist] / (float)_playerStatsReference.defaultPlayerStats[StatType.thrist];
			//_statsSliders[4].value = _playerStatsReference.consumedStats[StatType.sleep] / (float)_playerStatsReference.defaultPlayerStats[StatType.sleep];

			if (backPanel.color != _activeColor && _playerStatsReference.poisonLevel <= 0){
				 if (_statsSliders [0].value < _danger1 || _statsSliders [1].value < _danger1 || _statsSliders [2].value < _danger1 || _statsSliders [3].value < _danger1) {
					_actualColor = _dangerColor1;
				} else if (_statsSliders [0].value < _danger2 || _statsSliders [1].value < _danger2 || _statsSliders [2].value < _danger2 || _statsSliders [3].value < _danger2) {
					_actualColor = _dangerColor2;
				} else {
					_actualColor = _normalColor;
				}
			}

			if (_playerStatsReference.isDead)
			{
				backPanel.color = _deadColor;
				_playerImg.sprite = _deadSprite;
				StopAllCoroutines();
			}
		}

		/// <summary>
		/// Change the player
		/// </summary>
		/// <param name="val">current player</param>
		public void ChangePlayer(PlayerStatsReduction val)
		{
			if (_playerStatsReference == null)
				return;
			if (_playerStatsReference.name == val.name) {
				_actualColor = _activeColor;
			} else {
				if (_statsSliders [0].value < _danger1 || _statsSliders [1].value < _danger1 || _statsSliders [2].value < _danger1 || _statsSliders [3].value < _danger1) {
					_actualColor = _dangerColor1;
				} else if (_statsSliders [0].value < _danger2 || _statsSliders [1].value < _danger2 || _statsSliders [2].value < _danger2 || _statsSliders [3].value < _danger2) {
					_actualColor = _dangerColor2;
				} else {
					_actualColor= _normalColor;
				}

				if (_playerStatsReference.isDead)
				{
					backPanel.color = _deadColor;
					_playerImg.sprite = _deadSprite;
					StopAllCoroutines();
				}
			}
		}

		public IEnumerator Poisoned(){
			float val = 0;
			while (true) {
				while (_playerStatsReference.poisonLevel > 0 && val < 1) {
					yield return 0;
					backPanel.color = Color.Lerp (_actualColor, _poisonColor, val);
					val += Time.deltaTime;
				}

				while (_playerStatsReference.poisonLevel > 0 && val > 0) {
					yield return 0;
					backPanel.color = Color.Lerp (_actualColor, _poisonColor, val);
					val-= Time.deltaTime;
				}

				val = 0;
				backPanel.color = Color.Lerp (_actualColor, _poisonColor, val);
				yield return 0;
			}
			
		}
    }
}