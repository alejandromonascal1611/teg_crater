﻿using UnityEngine;
using System.Collections;

public class FloatCameraMovement : MonoBehaviour {

	public float speed;
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey(KeyCode.I))
			transform.position += Vector3.up * speed * Time.deltaTime;
		if (Input.GetKey(KeyCode.K))
			transform.position -= Vector3.up * speed * Time.deltaTime;
		if (Input.GetKey(KeyCode.J))
			transform.position -= Vector3.right * speed * Time.deltaTime;
		if (Input.GetKey(KeyCode.L))
			transform.position += Vector3.right * speed * Time.deltaTime;
		if (Input.GetKey(KeyCode.U))
			GetComponent<UnityEngine.Camera>().orthographicSize += speed  * 4 * Time.deltaTime;
		if (Input.GetKey(KeyCode.O))
			GetComponent<UnityEngine.Camera>().orthographicSize -= speed  * 4 * Time.deltaTime;
	}
}
