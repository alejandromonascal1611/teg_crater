﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace Camera
{
    /// <summary>
    /// Camera Movement
    /// </summary>
    public class CameraMovement : MonoBehaviour
    {
        [Header("General")]
        public float speed= 1;

        public float range= 1;
        
        public Transform currentPlayerPosition;
        
        [Header("Too Far Object")]
        public float tooFarRange = 1;

        public float animationTime = 1;

        public Image image;

        private float _relativeSpeed;

        void Awake() {
            _relativeSpeed = speed;
			Messenger.AddListener<Transform>(GlobalVariables.MESSAGE_ON_PLAYER_FOCUS, focusOnNewTransform);
			Messenger.MarkAsPermanent (GlobalVariables.MESSAGE_ON_PLAYER_FOCUS);
		}

		void OnDestroy(){
			Messenger.RemoveListener<Transform>(GlobalVariables.MESSAGE_ON_PLAYER_FOCUS, focusOnNewTransform);
		}

        void Update() {
            float distance= Mathf.Abs(Vector3.Magnitude(transform.position - currentPlayerPosition.position + Vector3.forward * 10));
            
            if (distance > range)
            {
                transform.position = Vector2.Lerp(transform.position, currentPlayerPosition.position, _relativeSpeed * Time.deltaTime);
                transform.position += Vector3.forward * -10;
            }

			if (distance > tooFarRange && _relativeSpeed == speed)
			{
				_relativeSpeed = speed / (distance - tooFarRange);
				StopAllCoroutines();
				StartCoroutine(blackOut());
			}
        }

        void focusOnNewTransform(Transform newTransform) {
            currentPlayerPosition = newTransform;
            //float distance = Mathf.Abs(Vector3.Magnitude(transform.position - currentPlayerPosition.position + Vector3.forward * 10));
            
        }

        IEnumerator blackOut() {
            while (image.color.a < 0.95f){
                image.color = Color.Lerp(image.color, Color.black, animationTime * Time.deltaTime);
                yield return 0;
            }
            image.color = Color.black;
            
            transform.position = currentPlayerPosition.position;
            transform.position += Vector3.forward * -10;
            _relativeSpeed = speed;

            yield return new WaitForSeconds(0.2f);

            while (image.color.a > 0.05f){
                image.color = Color.Lerp(image.color, Color.clear, animationTime * Time.deltaTime);
                yield return 0;
            }
            image.color = Color.clear;
        }
    }
}
