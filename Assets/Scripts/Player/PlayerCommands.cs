﻿using UnityEngine;
using System.Collections;

namespace Player
{
    /// <summary>
    /// All input handle in the game, this enum must be changed depending on the game input settings
    /// </summary>
    public enum INPUT { Horizontal, Vertical, Action, Jump, ChangeL, ChangeR, Inventory, Interact, Gravity, Teleport};

    /// <summary>
    /// All possible Input Triggers
    /// </summary>
    public enum TRIGGER { Button, ButtonDown, ButtonUp, Axis, AxisRaw };

    /// <summary>
    /// Struct for saving commands information
    /// </summary>
    [System.Serializable]
    public struct command
    {
        public string name;
        public INPUT input;
        public TRIGGER trigger;
        public FloatEvent action;
        [HideInInspector]
        public bool playOnNextCall;
    }
    
    /// <summary>
    /// Static methods for handling player commands
    /// </summary>
    public class PlayerCommands
    {
        /// <summary>
        /// Applies the command if it is active.
        /// </summary>
        /// <param name="myCommand">My command.</param>
        public static void applyCommand(command myCommand)
        {
            switch (myCommand.trigger)
            {
                case TRIGGER.Axis:
                    myCommand.action.Invoke(Input.GetAxis(myCommand.input.ToString()));
                    break;
                case TRIGGER.AxisRaw:
                    myCommand.action.Invoke(Input.GetAxisRaw(myCommand.input.ToString()));
                    break;
                case TRIGGER.Button:
                    if (myCommand.playOnNextCall || Input.GetButton(myCommand.input.ToString()))
                        myCommand.action.Invoke(0);
                    break;
			case TRIGGER.ButtonDown:
				if (myCommand.playOnNextCall || Input.GetButtonDown (myCommand.input.ToString ())) {
					myCommand.action.Invoke (0);
				}
                    break;
                case TRIGGER.ButtonUp:
                    if (myCommand.playOnNextCall || Input.GetButtonUp(myCommand.input.ToString()))
                        myCommand.action.Invoke(0);
                    break;
            }
            myCommand.playOnNextCall = false;
        }

        /// <summary>
        /// Checks if a command is activated
        /// </summary>
        /// <param name="myCommand">My Command</param>
        public static void checkForCommand(command myCommand)
        {
            switch (myCommand.trigger)
            {
                case TRIGGER.Axis:
                    myCommand.playOnNextCall = true;
                    break;
                case TRIGGER.AxisRaw:
                    myCommand.playOnNextCall = true;
                    break;
                case TRIGGER.Button:
                    if (Input.GetButton(myCommand.input.ToString()))
                        myCommand.playOnNextCall = true;
                    break;
                case TRIGGER.ButtonDown:
                    if (Input.GetButtonDown(myCommand.input.ToString()))
                        myCommand.playOnNextCall = true;
                    break;
                case TRIGGER.ButtonUp:
                    if (Input.GetButtonUp(myCommand.input.ToString()))
                        myCommand.playOnNextCall = true;
                    break;
            }
        }
    }
}