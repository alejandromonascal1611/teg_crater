﻿using UnityEngine;
using System;

namespace Player
{
	/// <summary>
	/// The players interact command
	/// </summary>
	public class PlayerInteract : MonoBehaviour
	{

		private Action<bool> interact;

        private PlayerInventory _inventory;

        private void Awake()
        {
            _inventory = GetComponent<PlayerInventory>();
        }

        /// <summary>
        /// Grab an object if interactable
        /// </summary>
        public void GrabObject(bool showOnUI= true)
		{
			if (interact != null)
				interact.Invoke(showOnUI);
		}

		/// <summary>
		/// Calls the on trigger exit event
		/// </summary>
		/// <param name="collision"></param>
		public void OnTriggerExit(Collider collision)
		{
			interact = null;
		}

		/// <summary>
		/// Calls the on trigger stay event
		/// </summary>
		/// <param name="collision"></param>
		public void OnTriggerStay(Collider collision)
		{
			interact = (showOnUI) => {
                if(collision != null)
                    collision.GetComponent<IInteractable>().Interact(showOnUI,_inventory);
                interact = null;
                };
		}
		
	}
}
