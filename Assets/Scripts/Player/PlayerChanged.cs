﻿using UnityEngine;
using System.Collections;
using IA;
using Stats;

namespace Player {
    /// <summary>
    /// Handles the change player event
    /// </summary>
    [RequireComponent(typeof(PlayerInput),typeof(PlayerIA))]
    public class PlayerChanged : MonoBehaviour {

        /// <summary>
        /// My Player Input Reference
        /// </summary>
        PlayerInput _playerInput;

        PlayerIA _playerIA;

        PlayerStatsReduction _stats;

        ///// <summary>
        ///// Starts this instance
        ///// </summary>
        //void Start() {
        //    _playerInput = GetComponent<PlayerInput>();
        //}

        /// <summary>
        /// On Player Toggle Event
        /// </summary>
        /// <param name="toggle">true if activates, false if desactivates</param>
        void OnPlayerToggle(bool toggle) {
            if (_stats == null)
                _stats = GetComponent<PlayerStatsReduction>();

			if(_playerInput == null)
				_playerInput = GetComponent<PlayerInput>();
			_playerInput.enabled = !_stats.isDead && toggle;

            if (_playerIA == null)
                _playerIA = GetComponent<PlayerIA>();
            _playerIA.enabled = !_stats.isDead && !toggle;
            _playerIA.SetEnable(!_stats.isDead && !toggle);

            //Assure that if it is dead jump to next player
            if(_stats.isDead && toggle)
                Messenger.Broadcast<float>(GlobalVariables.MESSAGE_ON_PLAYER_CHANGE, 1);
        }
    }
}