﻿using UnityEngine;
using System.Collections;

public class PlayerAnimations : MonoBehaviour
{

	/// <summary>
	/// Animators Reference
	/// </summary>
	Animator _animator;

	int hashID_IsWalking;
	//int hashID_IsTired;
	int hashID_IsJumping;

	SpriteRenderer _spriteRenderer;

	/// <summary>
	/// Awakes this instance
	/// </summary>
	public void Awake()
	{
		_animator = GetComponent<Animator>();
		hashID_IsWalking = Animator.StringToHash("IsWalking");
		//hashID_IsTired = Animator.StringToHash("IsTired");
		hashID_IsJumping = Animator.StringToHash("IsJumping");
		_spriteRenderer = GetComponent<SpriteRenderer>();
	}

	/// <summary>
	/// The movement action
	/// </summary>
	/// <param name="val">The horizontal axis</param>
	public void Move(float val)
	{
		if (Mathf.Abs(val) > Mathf.Epsilon)
			_spriteRenderer.flipX = val < 0;
		_animator.SetBool(hashID_IsWalking, Mathf.Abs(val) > Mathf.Epsilon);
	}

	/// <summary>
	/// The jump action
	/// </summary>
	public void Jump()
	{
		_animator.SetBool(hashID_IsJumping, true);
		StopAllCoroutines();
		StartCoroutine(checkForTheFloor());
	}

	/// <summary>
	/// Checks When the floor is hitted
	/// </summary>
	/// <returns></returns>
	IEnumerator checkForTheFloor()
	{
		Collider[] colls;
        yield return new WaitForSeconds(0.5f);
		while (_animator.GetBool(hashID_IsJumping))
		{
			yield return 0;
			colls = Physics.OverlapSphere(transform.position + Vector3.down * 0.5f, 0.45f);
			for (int i = 0; i < colls.Length; i++)
			{
				if (colls[i].tag == GlobalVariables.TAG_GROUND)
					_animator.SetBool(hashID_IsJumping, false);
			}
		}
	}
}
