﻿using UnityEngine;
using Inventory;
using Stats;
using System.Collections.Generic;
using Interactable;
using ProceduralGeneration;
using System.Collections;

namespace Player
{
	/// <summary>
	/// Handles the Players Inventory
	/// </summary>
	[RequireComponent(typeof(PlayerStatsReduction))]
	public class PlayerInventory : MonoBehaviour
	{
		/// <summary>
		/// Size of this inventory is constant
		/// </summary>
		public InventoryElement[] _inventoryElements;

		/// <summary>
		/// Temporal sprites
		/// </summary>
		//[SerializeField]
		//private Sprite[] _inventorySprites;

		/// <summary>
		/// Prefab for the inventory interactable
		/// </summary>
		[SerializeField]
		private GameObject _inventoryInteractablePrefab;

		/// <summary>
		/// Prefab for the electronic inventory interactable
		/// </summary>
		[SerializeField]
		private GameObject _electronicInventoryInteractablePrefab;

		/// <summary>
		/// Reference to the player stat reduction
		/// </summary>
		private PlayerStatsReduction _statsReduction;

		public PlayerType type;

		public string specialActionName;

		public ISpecialAction specialAction;

		public float[] timers;

		public float timerFactor;

		public IEnumerator[] countdowns;

		/// <summary>
		/// The data reference.
		/// </summary>
		public InteractableData waterRef;

		/// <summary>
		/// Awakes this instance
		/// </summary>
		void Awake()
		{
			_inventoryElements = new InventoryElement[6];
			countdowns = new IEnumerator[6];
			timers = new float[6];

			//TODO: Temporal all this should be created from a json
			Dictionary<InventoryEffectStats, int> rangeDictionary = new Dictionary<InventoryEffectStats, int>();
			rangeDictionary.Add(InventoryEffectStats.vitality, 10);
			rangeDictionary.Add(InventoryEffectStats.energy, 2);
			rangeDictionary.Add(InventoryEffectStats.hunger, 2);
			rangeDictionary.Add(InventoryEffectStats.thrist, 2);
			rangeDictionary.Add(InventoryEffectStats.sleep, 5);
			rangeDictionary.Add(InventoryEffectStats.duration, 20);
			rangeDictionary.Add(InventoryEffectStats.timeBeforeNext, 5);

			for (int i = 0; i < _inventoryElements.Length; i++)
			{
				countdowns [i] = Countdown.coutdown (0);
				_inventoryElements[i] = null;//new InventoryElement();
				//_inventoryElements[i].CreateItem(0, rangeDictionary);
				//TODO: Change this to an resources load
				//_inventoryElements[i].elementSprite = _inventorySprites[Random.Range(0, _inventorySprites.Length)];
			}
			_statsReduction = GetComponent<PlayerStatsReduction>();
		}

		/// <summary>
		/// Use inventory element
		/// </summary>
		/// <param name="index"></param>
		public bool UseInventoryElement(int index)
		{
			if (_inventoryElements[index] != null)
			{
				if (_inventoryElements [index].dataRef.name.Equals ("WaterDispenser")) {
					InventoryElement aux = new InventoryElement();
					aux.CreateItem(waterRef);
					aux.elementSprite = waterRef.sprite;
					aux.inventorySprite = waterRef.inventorySprite;
					if (InventoryUI.playerInventory.TryObtainInventoryElement(aux))
					{
						Messenger.Broadcast(GlobalVariables.MESSAGE_ON_UPDATE_INVENTORY);
						timers [index] = waterRef.timer;
						StopCoroutine (countdowns [index]);
						countdowns [index] = Countdown.coutdown (0);
						return true;
					}
				} else {
					StartCoroutine(_inventoryElements[index].applyEffectOverTime(_statsReduction));
					_inventoryElements[index] = null;
				}
			}
			return false;
		}

		/// <summary>
		/// Special Action
		/// </summary>
		/// <param name="index"></param>
		public void SpecialActionInventoryElement(int index, InventoryUI ui)
		{
			specialAction.Act (index, this, ui);
		}

		/// <summary>
		/// Drops inventory element
		/// </summary>
		/// <param name="index"></param>
		public bool DropInventoryElement(int index)
		{
			PlayerType[] types = _inventoryElements [index].dataRef.playerTypeInventoryActions;
			bool isMechanic = false;
			for (int j = 0; j < types.Length && !isMechanic; j++)
				isMechanic = (types [j] == PlayerType.mechanic);

			GameObject temp = (GameObject)Instantiate(isMechanic? _electronicInventoryInteractablePrefab :_inventoryInteractablePrefab, transform.position, Quaternion.identity);

			if (isMechanic)
				temp.transform.GetChild (0).gameObject.SetActive (!_inventoryElements [index].isSpecial);

			//temp.transform.localPosition += Vector3.up * temp.GetComponent<Renderer>().bounds.size.y * 0.5f;
			temp.GetComponent<InventoryInteractable>().DropInit(_inventoryElements[index]);
			Vector3 position = new Vector3(Mathf.Round(temp.transform.position.x)-0.5f,Mathf.Round(temp.transform.position.y),temp.transform.position.z);
			int count = 0;
			Vector3 size = new Vector3(temp.GetComponent<Renderer>().bounds.size.x * 0.5f, temp.GetComponent<Renderer>().bounds.size.y * 0.5f, temp.GetComponent<Renderer>().bounds.size.z * 0.5f);

			InteractablesGenerator refIG = FindObjectOfType<InteractablesGenerator>();
			LayerMask _floorLayers = refIG._floorLayers;
			LayerMask _interactLayers = refIG._floorLayers;
            while (Physics.OverlapBox(position + new Vector3(0, count), size, temp.transform.rotation, _floorLayers).Length > 0)
				count++;
			while (Physics.OverlapBox(position + new Vector3(0, count - 1), size, temp.transform.rotation, _floorLayers).Length == 0)
				count--;
			count--;

			if (Physics.OverlapBox (position + new Vector3 (0, count), size, temp.transform.rotation, _interactLayers).Length > 1) {
				Destroy (temp);
				//Play action not made sound
				return false;
			}
			
			temp.transform.position = position + new Vector3(0, count);


			return true;
		}

		/// <summary>
		/// Show info for this inventory item
		/// </summary>
		/// <param name="index"></param>
		public string AnalizeInventoryElement(int index)
		{
			if (_inventoryElements[index] != null)
				return (
					(_inventoryElements[index].stats[InventoryEffectStats.vitality] == 0 ? "" : "Vitality: " + _inventoryElements[index].stats[InventoryEffectStats.vitality] + "\n") +
					(_inventoryElements[index].stats[InventoryEffectStats.energy] == 0 ? "" : "Energy: " + _inventoryElements[index].stats[InventoryEffectStats.energy] + "\n") +
					(_inventoryElements[index].stats[InventoryEffectStats.hunger] == 0 ? "" : "Hunger: " + _inventoryElements[index].stats[InventoryEffectStats.hunger] + "\n") +
					(_inventoryElements[index].stats[InventoryEffectStats.thrist] == 0 ? "" : "Thrist: " + _inventoryElements[index].stats[InventoryEffectStats.thrist] + "\n") +
					(_inventoryElements[index].stats[InventoryEffectStats.sleep] == 0 ? "" : "Sleep: " + _inventoryElements[index].stats[InventoryEffectStats.sleep])
					);
			return null;
		}

		/// <summary>
		/// Tries to get a new item
		/// </summary>
		/// <param name="element">Eleemnt to be added</param>
		/// <returns><c>False</c> if item could not be obtained. <c>True</c> otherwise</returns>
		public bool TryObtainInventoryElement(InventoryElement element)
		{
			for (int i = 0; i < _inventoryElements.Length; i++)
			{
				if (_inventoryElements[i] == null)
				{
					_inventoryElements[i] = element;
					return true;
				}
			}
			return false;
		}

		public void continueTimers() {
			for (int i = 0; i < 6; i++) {
				if (timers [i] > Mathf.Epsilon) {
					countdowns [i] = Countdown.coutdown (timers [i]);
					StartCoroutine (countdowns [i]);
				}
			}
		}

		public void stopTimers() {
			for (int i = 0; i < 6; i++) {
				if (countdowns [i].Current != null) {
					timers [i] = (float)countdowns [i].Current;
					StopCoroutine (countdowns [i]);
				}
			}
		}
	}
}
