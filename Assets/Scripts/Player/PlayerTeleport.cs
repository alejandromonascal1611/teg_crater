﻿using UnityEngine;
using System.Collections;

public class PlayerTeleport : MonoBehaviour {

	[SerializeField]
	private GameObject _teleportPrefab;

	private GameObject _teleportReference;

	private Vector3 _initialPosition;

	private bool _teleportToStart;

	public void Awake(){
		_teleportToStart = true;
		_initialPosition = transform.position;
		_teleportReference = GameObject.Instantiate<GameObject> (_teleportPrefab);
		_teleportReference.SetActive (false);
	}

	public void Update(){
		if (!_teleportToStart && Vector2.Distance (_initialPosition, transform.position) > Vector2.Distance (_initialPosition, _teleportReference.transform.position)) {
			_teleportReference.SetActive (false);
			_teleportToStart = true;
		}	
	}

	public void Teleport(){
		if (_teleportToStart) {
			_teleportReference.SetActive (true);
			_teleportReference.transform.position = transform.position;
			transform.position = _initialPosition;
		} else {
			transform.position = _teleportReference.transform.position;
			_teleportReference.SetActive (false);
		}
		_teleportToStart = !_teleportToStart;
	}
}
