﻿using UnityEngine;
using System.Collections;

namespace Player
{
    /// <summary>
    /// All Player Movement in this Script
    /// </summary>
    [RequireComponent(typeof(Rigidbody))]
    public class PlayerMovement : MonoBehaviour
    {
        [Header("Movement Settings")]
        /// <summary>
        /// The acceleration 
        /// </summary>
        public float acceleration = 1f;
        
        /// <summary>
        /// The friction the wind makes
        /// </summary>
        public float friction = 0.5f;

        [Header("Jump Settings")]
        /// <summary>
        /// Strength of the jump
        /// </summary>
        public float jumpForce = 100f;

		private int gravityEffector = 0;

        /// <summary>
        /// This instance rigidbody
        /// </summary>
        private Rigidbody _rigidbody;

        /// <summary>
        /// Starts this instance
        /// </summary>
        void Start()
        {
            _rigidbody = GetComponent<Rigidbody>();
        }

        /// <summary>
        /// Fixed Update
        /// </summary>
        void FixedUpdate()
        {
            _rigidbody.velocity = new Vector2(_rigidbody.velocity.x * friction, _rigidbody.velocity.y);
        }

        /// <summary>
        /// The movement action
        /// </summary>
        /// <param name="val">The horizontal axis</param>
        public void Move(float val)
        {
			if (gravityEffector == 0)
				_rigidbody.velocity += Vector3.right * val * acceleration;
			else
				_rigidbody.velocity += Vector3.right * val * acceleration * 0.2f;// + Vector3.up*0.25f*Mathf.Abs(val);
        }

        /// <summary>
        /// The jump action
        /// </summary>
        public void Jump()
        {
			Collider[] colls = Physics.OverlapSphere(transform.position, 0.3f);
			for(int i=0; i< colls.Length; i++)
			{
				if (gravityEffector==1 || colls [i].tag == GlobalVariables.TAG_GROUND) {
					_rigidbody.velocity = new Vector3(_rigidbody.velocity.x, jumpForce, _rigidbody.velocity.z);
					return;
				}
			}
        }

		public void AffectGravity(){
			if (gravityEffector == 0) {
				jumpForce *= 1.5f;
				acceleration *= 0.75f;
			} else {
				jumpForce /= 1.5f;
				acceleration /= 0.75f;
			}
			gravityEffector = 1 - gravityEffector;

			/*if (gravityEffector == 0)
				gravityEffector = 1;
			else {
				Collider[] colls = Physics.OverlapSphere (transform.position, 0.3f);
				for (int i = 0; i < colls.Length; i++) {
					if (colls [i].tag == GlobalVariables.TAG_GROUND) {
						gravityEffector = 1 - gravityEffector;
						return;
					}
				}
			}*/
		}

		/// <summary>
		/// Change the player
		/// </summary>
		/// <param name="val">The direction for the change</param>
		public void ChangePlayer(float val)
		{
			Messenger.Broadcast <float>(GlobalVariables.MESSAGE_ON_PLAYER_CHANGE, val);
		}
    }
}