﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

namespace Player
{
    /// <summary>
    /// Handles all Player Input
    /// </summary>
    public class PlayerInput : MonoBehaviour
    {
        /// <summary>
        /// FixedUpdate Commands
        /// </summary>
        public command[] fixedUpdateCommands;

        /// <summary>
        /// LateUpdate Commands
        /// </summary>
        public command[] lateUpdateCommands;

        /// <summary>
        /// Update Commands
        /// </summary>
        public command[] updateCommands;

        /// <summary>
        /// Execute all FixedUpdate Commands Active
        /// </summary>
        void FixedUpdate()
        {
            for (int i = 0; i < fixedUpdateCommands.Length; i++)
            {
                PlayerCommands.applyCommand(fixedUpdateCommands[i]);
            }
        }

        /// <summary>
        /// Execute all Update Commands Active and Check for Fixed and Late Commands Activations
        /// </summary>
        void Update()
        {
            for (int i = 0; i < updateCommands.Length; i++)
            {
                PlayerCommands.applyCommand(updateCommands[i]);
            }
            for (int i = 0; i < fixedUpdateCommands.Length; i++)
            {
                PlayerCommands.checkForCommand(fixedUpdateCommands[i]);
            }
            for (int i = 0; i < lateUpdateCommands.Length; i++)
            {
                PlayerCommands.checkForCommand(lateUpdateCommands[i]);
            }
        }

        /// <summary>
        /// Execute all LateUpdate Commands Active
        /// </summary>
        void LateUpdate()
        {
            for (int i = 0; i < lateUpdateCommands.Length; i++)
            {
                PlayerCommands.applyCommand(lateUpdateCommands[i]);
            }
        }
    }
}