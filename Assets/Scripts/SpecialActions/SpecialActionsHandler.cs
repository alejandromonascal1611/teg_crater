﻿using UnityEngine;
using System.Collections;
using SpecialActions.OverInventory;

public class SpecialActionsHandler : Singleton<SpecialActionsHandler> {
	public ISpecialAction[] specialActionsReferences = {
		new Analize(),
		new Harvest(),
		new Ration(),
		new Cook(),
		new Fix(),
		new Antidote()
	};
}
