﻿using UnityEngine;
using System.Collections;
using Inventory;
using Player;
using UnityEngine.UI;

namespace SpecialActions.OverInventory{
	public class Harvest : ISpecialAction {

		public override void Act(params System.Object[] args){
			//Parameters
			int index = (int)args [0];
			PlayerInventory inventory = (PlayerInventory)args [1];
			InventoryUI ui = (InventoryUI)args [2];

			//Method
			if (inventory.DropInventoryElement (index)) {
				//inventory.timers[index] = inventory._inventoryElements[index].dataRef.timer * inventory.timerFactor; //20 segundos / spread

				//Add all previous timers
				for(int i = 0; i < inventory.timers.Length; i++)
					inventory.timers [index] += inventory.timers [i];
				//Use a factor
				inventory.timers [index] *= 0.5f; 
				//Add this item timer
				inventory.timers [index] += inventory._inventoryElements[index].dataRef.timer * inventory.timerFactor;

                if (ui != null)
                {
                    ui._inventoryElementOptions[index].SetActive(false);
                    ui.UpdateInventory();
                }
			}
		}
	}
}
