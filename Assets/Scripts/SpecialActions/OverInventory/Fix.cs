﻿using UnityEngine;
using System.Collections;
using Inventory;
using Player;
using UnityEngine.UI;

namespace SpecialActions.OverInventory{
	public class Fix : ISpecialAction {

		public override void Act(params System.Object[] args){
			//Parameters
			int index = (int)args [0];
			PlayerInventory inventory = (PlayerInventory)args [1];
			InventoryUI ui = (InventoryUI)args [2];

			//Method
			inventory.timers [index] = 0;//inventory._inventoryElements[index].dataRef.timer * inventory.timerFactor; //20 segundos / spread

			//Add all previous timers
			for(int i = 0; i < inventory.timers.Length; i++)
				inventory.timers [index] += inventory.timers [i];
			//Use a factor
			inventory.timers [index] *= 0.5f; 
			//Add this item timer
			inventory.timers [index] += inventory._inventoryElements[index].dataRef.timer * inventory.timerFactor;

			inventory.StartCoroutine (waitForFix (index, inventory, ui));

            if (ui != null)
            {
			    ui._inventoryElementOptions [index].SetActive (false);
                ui.UpdateInventory();
            }
		}

		public IEnumerator waitForFix(int index, PlayerInventory inventory, InventoryUI ui){
			yield return new WaitForSeconds (inventory.timers [index]);

			InventoryElement ie = inventory._inventoryElements[index];
			ie.elementSprite = ie.dataRef.specialSprite;
			ie.inventorySprite = ie.dataRef.specialInventorySprite;
			ie.description = ie.dataRef.specialDescription;

			if (ie.dataRef.name.StartsWith ("SpaceshipPart")) {
				inventory._inventoryElements [index] = null;
				Objectives.partsFixed++;
				Messenger.Broadcast<string> (GlobalVariables.MESSAGE_ON_RECEIVE_MESSAGE, "Spaceship Part Fixed: " + Objectives.partsFixed +" / " + Objectives.totalSpaceShipParts);
			}else
				inventory._inventoryElements [index] = ie;

			if (ie.dataRef.name.StartsWith ("SOS")) {
				Messenger.Broadcast<string> (GlobalVariables.MESSAGE_ON_RECEIVE_MESSAGE, "SOS Signal Amplified");
				Messenger.Broadcast (GlobalVariables.MESSAGE_REDUCE_TIME);
			}

			if (ie.dataRef.name.StartsWith ("Water")) {
				inventory._inventoryElements [index].isUsable = true;
			}

            if (ui != null)
            {
                ui._inventoryElementOptions[index].SetActive(false);
                ui.UpdateInventory();
            }
		}
	}
}
