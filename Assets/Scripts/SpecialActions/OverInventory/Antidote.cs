﻿using UnityEngine;
using System.Collections;
using Inventory;
using Player;
using UnityEngine.UI;

namespace SpecialActions.OverInventory{
	public class Antidote : ISpecialAction {

		public override void Act(params System.Object[] args){
			//Parameters
			int index = (int)args [0];
			PlayerInventory inventory = (PlayerInventory)args [1];
			InventoryUI ui = (InventoryUI)args [2];

			//Method
			inventory.timers [index] = 0;//inventory._inventoryElements[index].dataRef.timer * inventory.timerFactor; //20 segundos / spread

			//Add all previous timers
			for(int i = 0; i < inventory.timers.Length; i++)
				inventory.timers [index] += inventory.timers [i];
			//Use a factor
			inventory.timers [index] *= 0.5f; 
			//Add this item timer
			inventory.timers [index] += inventory._inventoryElements[index].dataRef.timer * inventory.timerFactor;

			inventory.StartCoroutine (waitForAntidote (index, inventory, ui));

            if (ui != null)
            {
                ui._inventoryElementOptions[index].SetActive(false);
                ui.UpdateInventory();
            }
		}

		public IEnumerator waitForAntidote(int index, PlayerInventory inventory, InventoryUI ui){
			yield return new WaitForSeconds (inventory.timers [index]);

			InventoryElement ie = inventory._inventoryElements[index];
			if (!ie.dataRef.name.Contains ("Water")) {
				
				for (int i = 0; i < 5; i++) {
					if (ie.stats.ContainsKey ((InventoryEffectStats)i))
						ie.stats [(InventoryEffectStats)i] = 0;
				}
			} else {
				ie.stats [InventoryEffectStats.energy] = 45;
			}

			ie.dataRef.poisonProb = 0;

			ie.elementSprite = ie.dataRef.antidoteSprite;
			ie.inventorySprite = ie.dataRef.antidoteSprite;
			ie.description = ie.dataRef.antidoteDescription;
			inventory._inventoryElements [index] = ie;

            if (ui != null)
            {
                ui._inventoryElementOptions[index].SetActive(false);
                ui.UpdateInventory();
            }
		}
	}
}
