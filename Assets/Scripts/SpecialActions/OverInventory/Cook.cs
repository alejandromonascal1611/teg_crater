﻿using UnityEngine;
using System.Collections;
using Inventory;
using Player;
using UnityEngine.UI;

namespace SpecialActions.OverInventory{
	public class Cook : ISpecialAction {

		public override void Act(params System.Object[] args){
			//Parameters
			int index = (int)args [0];
			PlayerInventory inventory = (PlayerInventory)args [1];
			InventoryUI ui = (InventoryUI)args [2];

			//Method
			inventory.timers [index] = 0;//inventory._inventoryElements[index].dataRef.timer * inventory.timerFactor; //20 segundos / spread

			//Add all previous timers
			for(int i = 0; i < inventory.timers.Length; i++)
				inventory.timers [index] += inventory.timers [i];
			//Use a factor
			inventory.timers [index] *= 0.5f; 
			//Add this item timer
			inventory.timers [index] += inventory._inventoryElements[index].dataRef.timer * inventory.timerFactor;

			inventory.StartCoroutine (waitForCook (index, inventory, ui));

            if (ui != null)
            {
                ui._inventoryElementOptions[index].SetActive(false);
                ui.UpdateInventory();
            }
		}

		public IEnumerator waitForCook(int index, PlayerInventory inventory, InventoryUI ui){
			yield return new WaitForSeconds (inventory.timers [index]);

			InventoryElement ie = inventory._inventoryElements[index];
//			for (int i = 0; i < 5; i++)
//			{
//				if (ie.stats.ContainsKey((InventoryEffectStats) i))
//					ie.stats[(InventoryEffectStats) i] = ie.stats[(InventoryEffectStats) i]*2;
//			}
			ie.stats[InventoryEffectStats.vitality]=  ie.dataRef.spVitality;
			ie.stats[InventoryEffectStats.energy]= ie.dataRef.spEnergy;
			ie.stats[InventoryEffectStats.hunger]= ie.dataRef.spHunger;
			ie.stats[InventoryEffectStats.thrist]= ie.dataRef.spThrist;
			ie.stats[InventoryEffectStats.sleep]= ie.dataRef.spSleep;

			ie.dataRef.poisonProb = 0;

			ie.elementSprite = ie.dataRef.specialSprite;
			ie.inventorySprite = ie.dataRef.specialInventorySprite;
			ie.description = ie.dataRef.specialDescription;
			inventory._inventoryElements [index] = ie;

            if (ui != null)
            {
                ui._inventoryElementOptions[index].SetActive(false);
                ui.UpdateInventory();
            }
        }
	}
}
