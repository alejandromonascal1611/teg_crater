﻿using UnityEngine;
using System.Collections;
using Inventory;
using Player;
using UnityEngine.UI;

namespace SpecialActions.OverInventory{
	public class Ration : ISpecialAction {

		public override void Act(params System.Object[] args){
			//Parameters
			int index = (int)args [0];
			PlayerInventory inventory = (PlayerInventory)args [1];
			InventoryUI ui = (InventoryUI)args [2];

			//Method
			InventoryElement ie = inventory._inventoryElements[index];
			for (int i = 0; i < 5; i++)
			{
				if (ie.stats.ContainsKey((InventoryEffectStats) i))
					ie.stats[(InventoryEffectStats) i] = ie.stats[(InventoryEffectStats) i]/2;
			}
			ie.elementSprite = ie.dataRef.rationSprite;
			ie.inventorySprite = ie.dataRef.rationInventorySprite;
			ie.description = ie.dataRef.rationDescription;
			inventory.UseInventoryElement (index);
            inventory._inventoryElements[index] = ie;
            if (ui != null)
            {
                ui._inventoryElementOptions[index].SetActive(false);
                ui.UpdateInventory();
            }
		}
	}
}
