﻿using UnityEngine;
using System.Collections;
using Inventory;
using Player;

namespace SpecialActions.OverInventory{
	public class Analize : ISpecialAction {

		public override void Act(params System.Object[] args){
			//Parameters
			int index = (int)args [0];
			PlayerInventory inventory = (PlayerInventory)args [1];
			InventoryUI ui = (InventoryUI)args [2];

			//Method
			string str = "";
			if (inventory._inventoryElements[index] != null)
				str = inventory._inventoryElements[index].description;

            if (ui != null)
            {
                ui.ShowAnalysis(index, str);
            }
		}
	}
}
