﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ManualHandler : MonoBehaviour {

	public ManualData[] manualData;

	public Text description;

	public Image sprite;

	private int pos = 0;

	private void setInfo(){
		description.text = manualData [pos].description;
		sprite.sprite = manualData [pos].sprite;
	}

	public void OnEnable(){
		pos = 0;
		setInfo ();
	}

	public void Next(){
		pos++;
		if (pos == manualData.Length)
			pos = 0;
		setInfo ();
	}

	public void Prev(){
		pos--;
		if (pos == -1)
			pos = manualData.Length - 1;
		setInfo ();
	}
}
