﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace ProceduralGeneration
{
	/// <summary>
	/// Generates the map using Cellular Automata
	/// </summary>
	public class MapGenerator : MonoBehaviour
	{
		#region Variables
		/// <summary>
		/// Seed used to generate the map
		/// </summary>
		public string seed;

		/// <summary>
		/// Width of the map
		/// </summary>
		[SerializeField]
		private int _width;

		/// <summary>
		/// Height of the map
		/// </summary>
		[SerializeField]
		private int _height;

		/// <summary>
		/// Teorical amount of the map to be filled 
		/// </summary>
		[SerializeField]
		[Range(0, 100)]
		private int _ranfomFillPercent;

		/// <summary>
		/// Size of each cell in the map
		/// </summary>
		[SerializeField]
		private float _cellSize;

		/// <summary>
		/// CellularRule to be used with the Map Generator
		/// </summary>
		[SerializeField]
		private CellularRule cellularRule;

		/// <summary>
		/// Contains the map on binary representation
		/// <c>0</c> Means empty space. <c>1</c> Means wall.
		/// </summary>
		private int[,] _map;

		#endregion

		/// <summary>
		/// Starts this instance
		/// </summary>
		//void Start()
		//{
		//	GenerateMap();
		//}

		#region GenerateMap

		/// <summary>
		/// Generates a Map
		/// </summary>
		public void GenerateMap()
		{
			_width += 20;
			_height += 20;
			_map = new int[_width, _height];
			RandomFillMap();

			CreateCentralExtension();

			for (int i = 0; i < cellularRule.iterarions; i++)
				CellularAutomata();

			FixSmallRegions();


			GetComponent<MeshGenerator>().GenerateMesh(_map, _cellSize * 0.5f);

			GetComponent<InteractablesGenerator>().CreateInteractables(seed, _width / 2, _height / 2);

			GetComponent<InteractablesGenerator>().CreateElectronics(seed, _width / 2, _height / 2);
		}

		/// <summary>
		/// Fill the map with random values
		/// </summary>
		void RandomFillMap()
		{

			if (seed == "")
				seed = DateTime.Now.ToString();
			System.Random rand = new System.Random(seed.GetHashCode());

			for (int i = 0; i < _width; i++)
			{
				for (int j = 0; j < _height; j++)
				{
					if (i < 10 || i > _width - 10 || j < 10 || j > _height - 10)
					{
						_map[i, j] = 1;
					}
					else
					{
						_map[i, j] = rand.Next(0, 100) < _ranfomFillPercent ? 1 : 0;
					}
				}
			}
		}

		/// <summary>
		/// Applies a cellular automata over the map
		/// </summary>
		void CellularAutomata()
		{
			int surroundingWallCount;
			for (int i = 0; i < _width; i++)
			{
				for (int j = 0; j < _height; j++)
				{
					surroundingWallCount = GetSurroundingWallCount(i, j);
					if (surroundingWallCount > cellularRule.setWallRule)
						_map[i, j] = 1;
					else if (surroundingWallCount < cellularRule.setEmptyRule)
						_map[i, j] = 0;
				}
			}
		}

		/// <summary>
		/// Counts how many walls surrounds a position
		/// </summary>
		/// <param name="gridX">Map position in the x coordinate</param>
		/// <param name="gridY">Map position in the y coordinate</param>
		/// <returns></returns>
		int GetSurroundingWallCount(int gridX, int gridY)
		{
			int wallCount = 0;
			for (int i = gridX - 1; i <= gridX + 1; i++)
			{
				for (int j = gridY - 1; j <= gridY + 1; j++)
				{
					if (IsInMapRange(i, j))
					{
						if (i != gridX || j != gridY)
						{
							wallCount += _map[i, j];
						}
					}
					else
						wallCount++;
				}
			}
			return wallCount;
		}

		#endregion

		#region FixMap

		void CreateCentralExtension(){
			System.Random rand = new System.Random(seed.GetHashCode());
			int y = rand.Next (_height / 4, _height / 3);
			int x = (_width - cellularRule.centralAreaExtension) / 2;
			for (int i = x; i < x + cellularRule.centralAreaExtension; i++) {
				for (int j = 3 * _height / 4; j > 0; j--) {
					_map [i, j] = j < y ? 1 : 0;
				}
			}
		}

		/// <summary>
		/// Fix the small regions problem
		/// </summary>
		void FixSmallRegions()
		{
			//Clean small rooms
			List<List<Coord>> roomRegions = GetRegions(0);
			List<List<Coord>> survivingRegions = new List<List<Coord>>();

			foreach (List<Coord> roomRegion in roomRegions)
			{
				if (roomRegion.Count < cellularRule.roomThresholdSize)
				{
					foreach (Coord tile in roomRegion)
					{
						_map[tile.tileX, tile.tileY] = 1;
					}
				}
				else
				{
					survivingRegions.Add(roomRegion);
				}
			}

			//Creates Passages
			List<Coord> representatives = new List<Coord>();
			System.Random rand = new System.Random(seed.GetHashCode());
			while (survivingRegions.Count > 1)
			{
				representatives.Clear();
				foreach (List<Coord> roomRegion in survivingRegions)
					representatives.Add(roomRegion[rand.Next(0, roomRegion.Count)]);

				int distance = 0;
				int maxDistance = int.MaxValue;
				int otherRoom;
				while (representatives.Count > 0)
				{
					maxDistance = int.MaxValue;
					otherRoom = -1;
					for (int j = 1; j < representatives.Count; j++)
					{
						if (distance < maxDistance)
						{
							maxDistance = distance;
							otherRoom = j;
						}
					}

					//If there is a connection create passage
					if (otherRoom != -1)
					{
						//Debug.DrawLine(CoordToWorldPoint(representatives[0]), CoordToWorldPoint(representatives[otherRoom]), Color.green, 100);
						float dx = representatives[otherRoom].tileX - representatives[0].tileX;
						float dy = representatives[otherRoom].tileY - representatives[0].tileY;
						float m = dy / dx;
						int step = representatives[0].tileX > representatives[otherRoom].tileX ? -1 : 1;
						int imgX = 0;
						float y;
						for (int x = representatives[0].tileX; x != representatives[otherRoom].tileX; x += step)
						{
							y = m * imgX + representatives[0].tileY;
							_map[x, Mathf.RoundToInt(y)] = 0;
							if(Mathf.RoundToInt(y) + 1 < _height - 1)
								_map[x, Mathf.RoundToInt(y) + 1] = 0;
							if (Mathf.RoundToInt(y) - 1 > 0)
								_map[x, Mathf.RoundToInt(y) - 1] = 0;
							imgX += step;
						}

						representatives.RemoveAt(otherRoom);
					}
					representatives.RemoveAt(0);
				}

				survivingRegions = GetRegions(0);
			}

			//Clean small walls
			List<List<Coord>> wallRegions = GetRegions(1);
			foreach (List<Coord> wallRegion in wallRegions)
			{
				if (wallRegion.Count < cellularRule.wallThresholdSize)
				{
					foreach (Coord tile in wallRegion)
					{
						_map[tile.tileX, tile.tileY] = 0;
					}
				}
			}

		}

		/// <summary>
		/// Get All the regions of an especific type
		/// </summary>
		/// <param name="tileType">The tileType that needs to be found their regions</param>
		/// <returns>All the regions of an especific type</returns>
		List<List<Coord>> GetRegions(int tileType)
		{
			List<List<Coord>> regions = new List<List<Coord>>();
			int[,] mapFlags = new int[_width, _height];

			for (int x = 0; x < _width; x++)
			{
				for (int y = 0; y < _height; y++)
				{
					if (mapFlags[x, y] == 0 && _map[x, y] == tileType)
					{
						List<Coord> newRegion = GetRegionTiles(x, y, ref mapFlags);
						regions.Add(newRegion);
					}
				}
			}

			return regions;
		}

		/// <summary>
		/// Use fill algorithm to get a region
		/// </summary>
		/// <param name="startX"></param>
		/// <param name="startY"></param>
		/// <param name="mapFlags"></param>
		/// <returns></returns>
		List<Coord> GetRegionTiles(int startX, int startY, ref int[,] mapFlags)
		{
			List<Coord> tiles = new List<Coord>();
			int tileType = _map[startX, startY];

			Queue<Coord> queue = new Queue<Coord>();
			queue.Enqueue(new Coord(startX, startY));
			mapFlags[startX, startY] = 1;

			while (queue.Count > 0)
			{
				Coord tile = queue.Dequeue();
				tiles.Add(tile);

				for (int x = tile.tileX - 1; x <= tile.tileX + 1; x++)
				{
					for (int y = tile.tileY - 1; y <= tile.tileY + 1; y++)
					{
						if (IsInMapRange(x, y) && (y == tile.tileY || x == tile.tileX))
						{
							if (mapFlags[x, y] == 0 && _map[x, y] == tileType)
							{
								mapFlags[x, y] = 1;
								queue.Enqueue(new Coord(x, y));
							}
						}
					}
				}
			}

			return tiles;
		}

		/// <summary>
		/// Cheks if a coordinate is inside the map
		/// </summary>
		/// <param name="x">X position</param>
		/// <param name="y">Y position</param>
		/// <returns>True if position is valid, False otherwise</returns>
		bool IsInMapRange(int x, int y)
		{
			return x >= 0 && x < _width && y >= 0 && y < _height;
		}

		/// <summary>
		/// Structure for the fill algorithm
		/// </summary>
		public struct Coord
		{
			public int tileX;
			public int tileY;

			public Coord(int x, int y)
			{
				tileX = x;
				tileY = y;
			}
		}

		/// <summary>
		/// Transforms from Coord to real world point
		/// </summary>
		/// <param name="tile">Coord</param>
		/// <returns>Real World Point</returns>
		Vector3 CoordToWorldPoint(Coord tile)
		{
			return new Vector3((-_width + _cellSize) * 0.5f + tile.tileX, (-_height + _cellSize) * 0.5f + tile.tileY, 0);
		}

		#endregion

		/// <summary>
		/// Draws Gizmo to see the data in the game
		/// </summary>
		//public void OnDrawGizmos()
		//{
		//	if (_map != null)
		//	{
		//		Gizmos.color = Color.white;
		//		Vector2 pos;
		//		for (int i = 0; i < _width; i++)
		//		{
		//			for (int j = 0; j < _height; j++)
		//			{
		//				pos = new Vector2((-_width + _cellSize) * 0.5f + i, (-_height + _cellSize) * 0.5f + j);
		//				if (_map[i, j] == 1)
		//					Gizmos.DrawCube(pos, Vector3.one * _cellSize);
		//			}
		//		}
		//	}
		//}
	}
}
