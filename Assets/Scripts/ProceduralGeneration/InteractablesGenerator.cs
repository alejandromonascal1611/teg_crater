﻿using UnityEngine;
using System.Collections;
using Interactable;

namespace ProceduralGeneration
{
	/// <summary>
	/// Generates the interactables in the map
	/// </summary>
	public class InteractablesGenerator : MonoBehaviour
	{
		public InteractableData[] interactables;

		public InteractableData[] electronics;

		/// <summary>
		/// Interactable Prefab Reference
		/// </summary>
		public GameObject interactablePrefab;

		/// <summary>
		/// Interactable Prefab Reference
		/// </summary>
		public GameObject electronicsInteractablePrefab;

		/// <summary>
		/// Used to encapsulate all the interactables
		/// </summary>
		public Transform parent;

		/// <summary>
		/// Min amount of interactables
		/// </summary>
		[SerializeField]
		private int _minValue;

		/// <summary>
		/// Max amount of interactables
		/// </summary>
		[SerializeField]
		private int _maxValue;

		[SerializeField]
		public LayerMask _floorLayers;

		[SerializeField]
		private LayerMask _interactLayers;

		/// <summary>
		/// Creates the interactables objects in the map
		/// </summary>
		/// <param name="seed">seed to be used in the creation of the interactables objects</param>
		public void CreateInteractables(string seed, int halfWidth, int halfHeight)
		{
			System.Random rand = new System.Random(seed.GetHashCode());

			int amount = rand.Next(_minValue, _maxValue);

			GameObject temp = null;
			Vector3 position;
			int count;
			int randValue;
			for (int i=0; i < amount; i++)
			{
				count = 0;
				position = new Vector3(rand.Next(-halfWidth + 20, halfWidth - 20) + 0.5f, rand.Next(-halfHeight + 1, 0));

				randValue = rand.Next (0, interactables.Length);
				if (temp == null)
				{
					temp = (GameObject)Instantiate(interactablePrefab, position + new Vector3(0, count), Quaternion.identity);
					temp.GetComponent<InventoryInteractable>().Init(interactables[randValue]);
					temp.transform.SetParent(parent, true);
				}

				//position -= new Vector3(0, temp.GetComponent<Renderer>().bounds.size.y * 0.5f, 0);

				Vector3 size = new Vector3(temp.GetComponent<Renderer>().bounds.size.x * 0.5f, temp.GetComponent<Renderer>().bounds.size.y * 0.5f, temp.GetComponent<Renderer>().bounds.size.z * 0.5f);

				while (Physics.OverlapBox(position + new Vector3(0, count), size, temp.transform.rotation, _floorLayers).Length > 0)
					count++;
				while (Physics.OverlapBox(position + new Vector3(0, count - 1), size, temp.transform.rotation, _floorLayers).Length == 0 && Mathf.Abs(count + position.y) <= halfWidth)
					count--;

				count--;
				//Don't create if out of domain
				if (Mathf.Abs(count + position.y) > halfWidth)
					continue;
				if (Physics.OverlapBox(position + new Vector3(0, count), size, temp.transform.rotation, _interactLayers).Length > 0)
					continue;

				temp.transform.position = position + new Vector3(0, count);

				for(int j = 1; j < interactables[randValue].maxSpread; j++){
					count = 0;
					while (Physics.OverlapBox(position + new Vector3(j, count), size, temp.transform.rotation, _floorLayers).Length > 0)
						count++;
					while (Physics.OverlapBox(position + new Vector3(j, count - 1), size, temp.transform.rotation, _floorLayers).Length == 0 && Mathf.Abs(count + position.y) <= halfWidth)
						count--;

					count--;
					//Don't create if out of domain
					if (Mathf.Abs (count + position.y) > halfWidth) {
						j = interactables [randValue].maxSpread;
						continue;
					}
					if (Physics.OverlapBox(position + new Vector3(0, count), size, temp.transform.rotation, _interactLayers).Length > 0) {
						j = interactables [randValue].maxSpread;
						continue;
					}

					temp = (GameObject)Instantiate(interactablePrefab, position + new Vector3(j, count), Quaternion.identity);
					temp.GetComponent<InventoryInteractable>().Init(interactables[randValue]);
					temp.transform.SetParent(parent, true);
				}

				temp = null;
            }
		}

		/// <summary>
		/// Creates the electronics objects in the map
		/// </summary>
		/// <param name="seed">seed to be used in the creation of the electronics objects</param>
		public void CreateElectronics(string seed, int halfWidth, int halfHeight)
		{
			System.Random rand = new System.Random(seed.GetHashCode());

			int amount = electronics.Length;

			GameObject temp = null;
			Vector3 position;
			int count;

			for (int i=0; i < amount; i+= temp == null ? 1 : 0)
			{
				count = 0;
				position = new Vector3(rand.Next(-halfWidth + 1, halfWidth - 1) + 0.5f, rand.Next(-halfHeight + 1, 0));

				if (temp == null)
				{
					temp = (GameObject)Instantiate(electronicsInteractablePrefab, position + new Vector3(0, count), Quaternion.identity);
					temp.GetComponent<InventoryInteractable>().Init(electronics[i]);
					temp.transform.SetParent(parent, true);
				}

				//position -= new Vector3(0, temp.GetComponent<Renderer>().bounds.size.y * 0.5f, 0);

				Vector3 size = new Vector3(temp.GetComponent<Renderer>().bounds.size.x * 0.5f, temp.GetComponent<Renderer>().bounds.size.y * 0.5f, temp.GetComponent<Renderer>().bounds.size.z * 0.5f);

				while (Physics.OverlapBox(position + new Vector3(0, count), size, temp.transform.rotation, _floorLayers).Length > 0)
					count++;
				while (Physics.OverlapBox(position + new Vector3(0, count - 1), size, temp.transform.rotation, _floorLayers).Length == 0 && Mathf.Abs(count + position.y) <= halfHeight)
					count--;

				count--;
				//Don't create if out of domain
				if (Mathf.Abs (count + position.y) > halfHeight) 
					continue;
				if (Physics.OverlapBox(position + new Vector3(0, count), size, temp.transform.rotation, _interactLayers).Length > 0)
					continue;

				temp.transform.position = position + new Vector3(0, count);

				temp = null;
			}
		}
	}

}
