﻿namespace ProceduralGeneration
{
	[System.Serializable]
	public struct CellularRule
	{
		public int iterarions;
		public int setWallRule;
		public int setEmptyRule;
		public int wallThresholdSize;
		public int roomThresholdSize;
		public int centralAreaExtension;
	}
}
