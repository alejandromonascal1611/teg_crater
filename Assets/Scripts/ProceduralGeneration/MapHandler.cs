﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace ProceduralGeneration
{
	/// <summary>
	/// Handles the map generator for an extended map
	/// </summary>
	public class MapHandler : MonoBehaviour
	{

		/// <summary>
		/// Enum for the direction the player is going
		/// </summary>
		public enum Direction { left = -1, right = 1 }

		/// <summary>
		/// Use the data of the previous World if it exist, if don't create new data
		/// </summary>
		public bool usePreviousWorld;

		/// <summary>
		/// The current area
		/// </summary>
		private int _currentArea;

		/// <summary>
		/// Used as a pool for not recreating the map generator object
		/// </summary>
		public MapGenerator _map;
		
		/// <summary>
		/// Starts the Map Handler
		/// </summary>
		public void Start()
		{
			if (!usePreviousWorld)
				PlayerPrefs.DeleteAll();
			_currentArea = 0;
			GetArea();
		}

		/// <summary>
		/// Creates an area
		/// </summary>
		/// <param name="dir"></param>
		public void GetArea()
		{
			string seed = PlayerPrefs.GetString("Seed_" + _currentArea, "");
			if (seed == "")
			{
				seed = DateTime.Now.ToString();
				PlayerPrefs.SetString("Seed_" + _currentArea, seed);
			}
			_map.seed = seed;
			_map.GenerateMap();
        }
	}
}
