﻿using UnityEngine;
using System.Collections.Generic;

namespace ProceduralGeneration
{
	/// <summary>
	/// Generates the mesh
	/// </summary>
	public class MeshGenerator : MonoBehaviour
	{
		[SerializeField]
		Material material;

		/// <summary>
		/// Vertices
		/// </summary>
		List<Vector3> vertices;

		/// <summary>
		/// Triangles
		/// </summary>
		List<int> triangles;

		/// <summary>
		/// Generates the mesh
		/// </summary>
		/// <param name="map">The map to be used in the generation of the mesh</param>
		/// <param name="cellHalfSize">Half size of the cell</param>
		public void GenerateMesh(int[,] map, float cellHalfSize)
		{
			vertices = new List<Vector3>();
			triangles = new List<int>();

			TrianglePoint[,] triangleMap = generateTriangleMap(map, cellHalfSize);

			for(int i=0; i< map.GetLength(0); i++)
			{
				for (int j = 0; j < map.GetLength(1); j++)
				{
					if(map[i,j] == 1)
						MeshFromPoints(triangleMap[i, j], triangleMap[i, j + 1], triangleMap[i + 1, j + 1], triangleMap[i + 1, j]);
				}
			}

			Mesh mesh = new Mesh();

			mesh.vertices = vertices.ToArray();
			mesh.triangles = triangles.ToArray();

			Vector2[] myUVs = new Vector2[vertices.Count]; // Create array with the same element count
			for(int i = 0; i<vertices.Count; i++)
			{
				myUVs[i] = new Vector2(vertices[i].x*0.5f,vertices[i].y*0.5f);
			}

			mesh.uv = myUVs;
			mesh.RecalculateNormals();

			GetComponent<MeshFilter>().mesh = mesh;
			GetComponent<MeshCollider>().sharedMesh = mesh;
			GetComponent<MeshRenderer> ().material = material;
		}

		/// <summary>
		/// Generates the traingle map
		/// </summary>
		/// <param name="map"></param>
		/// <param name="cellHalfSize"></param>
		/// <returns></returns>
		TrianglePoint[,] generateTriangleMap(int[,] map, float cellHalfSize)
		{
			TrianglePoint[,] trianglesMap = new TrianglePoint[map.GetLength(0)+1, map.GetLength(1)+1];
			Vector3 pos = Vector3.zero;
			int i, j;

			for(i = 0; i < map.GetLength(0); i++)
			{
				for(j = 0; j < map.GetLength(1); j++)
				{
					pos = new Vector3(-map.GetLength(0) * 0.5f + cellHalfSize + i, -map.GetLength(1) * 0.5f + cellHalfSize + j);
					trianglesMap[i, j] = (new TrianglePoint(pos + new Vector3(-cellHalfSize, -cellHalfSize)));
				}
				trianglesMap[i, j] = (new TrianglePoint(pos + new Vector3(-cellHalfSize, cellHalfSize)));
			}

			for (j = 0; j < map.GetLength(1); j++)
			{
				pos = new Vector3(-map.GetLength(0) * 0.5f + cellHalfSize + (i - 1), -map.GetLength(1) * 0.5f + cellHalfSize + j);
				trianglesMap[i, j] = (new TrianglePoint(pos + new Vector3(cellHalfSize, -cellHalfSize)));
			}
			trianglesMap[i, j] = (new TrianglePoint(pos + new Vector3(cellHalfSize, cellHalfSize)));

			return trianglesMap;
		}

		/// <summary>
		/// Creates a mesh from the points
		/// </summary>
		/// <param name="points">Points to create the mesh</param>
		void MeshFromPoints(params TrianglePoint[] points)
		{
			AssignVertices(points);

			CreateTriangle(points[0], points[1], points[2]);
			CreateTriangle(points[0], points[2], points[3]);
		}

		/// <summary>
		/// Creates the triangle indexes
		/// </summary>
		/// <param name="a">First Point</param>
		/// <param name="b">Second Point</param>
		/// <param name="c">Third Point</param>
		void CreateTriangle(TrianglePoint a, TrianglePoint b, TrianglePoint c)
		{
			triangles.Add(a.vertexIndex);
			triangles.Add(b.vertexIndex);
			triangles.Add(c.vertexIndex);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="points"></param>
		void AssignVertices(TrianglePoint[] points)
		{
			for (int i = 0; i < points.Length; i++)
			{
				if (points[i].vertexIndex == -1)
				{
					points[i].vertexIndex = vertices.Count;
					vertices.Add(points[i].position);
				}
			}
		}
	}

	public class TrianglePoint
	{
		public Vector3 position;
		public int vertexIndex;

		public TrianglePoint(Vector3 position)
		{
			this.position = position;
			vertexIndex = -1;
		}
	}
}
