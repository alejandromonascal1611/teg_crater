﻿using UnityEngine;
using Inventory;
using Player;
using System.Collections.Generic;

namespace Interactable
{
	/// <summary>
	/// Inventory Interactable
	/// </summary>
	public class InventoryInteractable : MonoBehaviour, IInteractable
	{
		/// <summary>
		/// Reference to the inventory Element
		/// </summary>
		private InventoryElement _inventoryElement;

		
		/// <summary>
		/// Temporal sprites
		/// </summary>
		//[SerializeField]
		//private Sprite[] _inventorySprites;

		/// <summary>
		/// Awakes this instance
		/// </summary>
//		public void Init(int seed)
//		{
//			if (_inventoryElement == null)
//			{
//				//TODO: Temporal all this should be created from a json
//				Dictionary<InventoryEffectStats, int> rangeDictionary = new Dictionary<InventoryEffectStats, int>();
//				rangeDictionary.Add(InventoryEffectStats.vitality, 10);
//				rangeDictionary.Add(InventoryEffectStats.energy, 2);
//				rangeDictionary.Add(InventoryEffectStats.hunger, 2);
//				rangeDictionary.Add(InventoryEffectStats.thrist, 2);
//				rangeDictionary.Add(InventoryEffectStats.sleep, 5);
//				rangeDictionary.Add(InventoryEffectStats.duration, 20);
//				rangeDictionary.Add(InventoryEffectStats.timeBeforeNext, 5);
//				
//				_inventoryElement = new InventoryElement();
//				_inventoryElement.CreateItem(seed, rangeDictionary);
//
//				//TODO: Change this to an resources load
//				//_inventoryElement.elementSprite = _inventorySprites[Random.Range(0, _inventorySprites.Length)];
//			}
//			GetComponent<SpriteRenderer>().sprite = _inventoryElement.elementSprite;
//        }

		public void Init(InteractableData data)
		{
			if(_inventoryElement == null)
			{
				_inventoryElement = new InventoryElement();
				_inventoryElement.CreateItem(data);
				_inventoryElement.elementSprite = data.sprite;
				_inventoryElement.inventorySprite = data.inventorySprite;
			}
			GetComponent<SpriteRenderer>().sprite = data.sprite;
		}

		/// <summary>
		/// Initialize a drop inventory element
		/// </summary>
		/// <param name="_inventoryElement"></param>
		public void DropInit(InventoryElement _inventoryElement) {
			this._inventoryElement = _inventoryElement;
			GetComponent<SpriteRenderer>().sprite = _inventoryElement.elementSprite;
		}

		/// <summary>
		/// Action of interacting with an interactable object
		/// </summary>
		public void Interact(bool showOnUI, PlayerInventory inventory)
		{
			if (inventory.TryObtainInventoryElement(_inventoryElement))
			{
                if(showOnUI)
    				Messenger.Broadcast(GlobalVariables.MESSAGE_ON_UPDATE_INVENTORY);
				Destroy(gameObject);
			}
		}
	}
}
